#ifndef PROTO_PC_H
#define PROTO_PC_H

#include "stm32l4xx_hal.h"
#include "usart.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern unsigned char  checkSumPosPC;
extern unsigned char  secretCodePC;
extern unsigned char  offuscatorPosPC;
extern unsigned char  productCodePC;

void    sendUsart1 ( uint8_t * str );
void 		sendUsart2 ( uint8_t *ptr );
void    putCharUsart_PC ( unsigned char dato );
uint8_t GetCharUsart_PC ( char * dato );
void    usart_PC_init ( void );
void    removeRXbufferPC ( void );
int     usart_PC ( void );
void    SendBufferUsart_PC ( void );                /* send one byte from output buffer */
void    SendResponseUsart_PC ( PCP_sResponse *sResp );
void    prepareUsart_PC ( unsigned int uwStartOfMessage );
void    decodeStringUsart_PC ( void );

#define RESPPREPAREPC(stat,leng)   {sResponsePC.ubSts = stat; sResponsePC.ubLen= leng;} /* save data for response to PC */


#endif

