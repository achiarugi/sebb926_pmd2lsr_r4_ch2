#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "stm32l4xx_hal.h"
#include "spi_adc_ltc1864l.h"
#include "dateEpoch.h"
#include "taskManager.h"
#include "version.h"
#include "eprom_address.h"
#include "timer.h"
#include "main.h"
#include "ad.h"
#include "out.h"
//#include "functions.h"
#include "usart_pc.h"
#include "spi_address.h"
#include "vee.h"
#include "sensit.h" 
#include "math.h"
#include "i2c_24AA512.h"
#include "B2B_MASTER_Laser.h"
#include "i2c_digitalpot_AD5243.h"

// 6.26ms transfer 256 byte with  SPI_BAUDRATEPRESCALER_256 
// 782us 256 byte con SPI_BAUDRATEPRESCALER_32
// 98us 256 byte con SPI_BAUDRATEPRESCALER_4
#define         DIM_SHARED_MEMORY       64
#define SIZEBUFRXTX		4096
#define POSCRC		    SIZEBUFRXTX-1
uint8_t toCPU_TxData[SIZEBUFRXTX];
uint8_t fromCPU_RxData[SIZEBUFRXTX];
uint32_t uwCRCValue;
uint8_t  sharedMemoryReceive[DIM_SHARED_MEMORY];
uint8_t  sharedMemoryTransmit[DIM_SHARED_MEMORY];
uint32_t sharedMemoryCRC=0;
uint32_t sharedMemoryLastRX=0;
uint16_t flgSpiLaserCommunicationEnd=0;
extern SPI_HandleTypeDef hspi1;
extern RTC_HandleTypeDef hrtc;
extern CRC_HandleTypeDef hcrc;

float tLasDef;
float iLasHSDef;
float iLasLSDef;
float iAMPHSDef;
float iAMPLSDef;
uint16_t lTriggDef;
uint16_t nSamplesDef;
uint16_t nMeanDef;
uint16_t lDCDef;
uint16_t iScanFreqDef;
uint8_t counterM;
uint8_t amplResistorValueDef;
void ( *ptrTaskManager ) ( void );
 //-------------------------------------------------------------------

void powerUp_state( void ){

  realTime.laserState=10;
  TC_OFF();    VLD_OFF();
//  LED1_ON();    LED2_ON();    LED3_ON();    LED4_ON();    LED5_ON();
  HAL_Delay(250);
//  LED1_OFF();    LED2_OFF();    LED3_OFF();    LED4_OFF();    LED5_OFF();
	
  RoadRunner.errorFlag=OFF;
  measurement.ready=0;  realTime.zero=5;  counterM=0; // controllare se sono sempre attive
	service.triggerRampa=30;
	measurement.autoRange=1;
	measurement.switchRange=0; realTime.sensitivityLevel=HIGH;realTime.concValue=0.0f;
	measurement.centerFlag=ON;
	measurement.amplResistorValue=32;
	measurement.autoBackground=1;
	measurement.zerophd=0;
  //********************************************************************************************************  
  iLasHSDef=laser.currentHSW;    tLasDef=laser.temperatureW;    lTriggDef=laser.triggerLevelHSW	;
  nSamplesDef=measurement.nAcquisitionPointsW;    nMeanDef=measurement.nAvgW;
  iAMPHSDef=laser.rampAmplitudeHSW;    iScanFreqDef=laser.rampFrequencyW;    lDCDef=measurement.dCLevelW;  
  iLasLSDef=laser.currentHSW;  iAMPLSDef=laser.rampAmplitudeLSW;  amplResistorValueDef=measurement.amplResistorValue;
  //********************************************************************************************************  
	
  set_ad5693_laser_temperature(laser.temperatureW);                                    
  TC_ON();    
  set_ad5693_laser_temperature(laser.temperatureW); 
  laser.tecStatus=ON;
  timCiclo1ms = Tc_housekeeping_state;
  ptrTaskManager = warming_housekeeping_state;
	
}

void warming_housekeeping_state(void){
  realTime.laserState=20;
  //LED3_OFF();
  if ( timCiclo1ms )         
        return;
  timCiclo1ms = Tc_warming_state;
  read_laser_temperature();
  read_laser_voltage();

  read_laser_current();
  read_peltier_current();
  read_cell_temperature();
  if (1){
    if (laser.laserStatus==OFF){
      if(RoadRunner.errorFlag==ON && ((laser.temperatureR>laser.temperatureW+200) ||
                                     (laser.temperatureR<laser.temperatureW-200))){
        ptrTaskManager = warming_housekeeping_state;
        service.nStab++;
        if (service.nStab>250){
          ptrTaskManager = error_state;
          realTime.errorType=1;
        }
      }
      else{
        ptrTaskManager = set_parameters_state;
        service.nStab=0;
      }
    }else{
      if(RoadRunner.errorFlag==ON && ((laser.temperatureR>laser.temperatureW+200) ||
                                     (laser.temperatureR<laser.temperatureW-200))){
        ptrTaskManager = warming_housekeeping_state;
        service.nStab++;
        if (service.nStab>250){
          ptrTaskManager = error_state;
          realTime.errorType=2;
        }
      }
      else{
        service.nStab=0;
        timCiclo1ms = Tc_measurement_state;
        ptrTaskManager = measurement_state;
      }
      if (RoadRunner.errorFlag==ON && (laser.voltageR<0.5f || laser.voltageR>2.8f)){
        ptrTaskManager = error_state;
        realTime.errorType=3;
      }
      if (RoadRunner.errorFlag==ON && (laser.currentLaserR<0.001f)){
        ptrTaskManager = error_state;
        realTime.errorType=4;
      }      
    }
  }
}

void error_state(){
  realTime.laserState=210;
  VLD_OFF();
  TC_OFF();
  HAL_Delay(250);
   HAL_Delay(250);
  ptrTaskManager=taskSPI_communication;
  ptrTaskManager = communication_state;
}

void set_sensitivity_level(void){
	
	if(realTime.sensitivityLevel==HIGH && measurement.autoRange==1 && (measurement.rampStatus==3 || realTime.concValue>3300 || realTime.bit<8)){
		set_ad5693_laser_bias_current(laser.currentLSW);
		set_laser_scan_amplitude(laser.rampAmplitudeLSW);
		realTime.sensitivityLevel=LOW;
	}	else if(realTime.sensitivityLevel==LOW  && measurement.autoRange==1 && realTime.concValue<3000 ){
		set_ad5693_laser_bias_current(laser.currentHSW);
		set_laser_scan_amplitude(laser.rampAmplitudeHSW);
		realTime.sensitivityLevel=HIGH;
	}
	if(realTime.sensitivityLevel==HIGH && measurement.switchRange==1 ){
		set_ad5693_laser_bias_current(laser.currentLSW);
		set_laser_scan_amplitude(laser.rampAmplitudeLSW);	
		realTime.sensitivityLevel=LOW;
		measurement.switchRange=0;
	}
	if(realTime.sensitivityLevel==LOW  && measurement.switchRange==1){
		set_ad5693_laser_bias_current(laser.currentHSW);
		set_laser_scan_amplitude(laser.rampAmplitudeHSW);
		realTime.sensitivityLevel=HIGH;
		measurement.switchRange=0;
	}
}

void set_parameters_state(void){
    realTime.laserState=30;
    ad5696_set_channel(WRITE_TO_AND_UPDT_DAC_N | AD5696_DAC_A | AD5696_DAC_B | AD5696_DAC_C | AD5696_DAC_D, 65535);  
                                 
    set_ad5693_laser_bias_current(laser.currentHSW);
    //set_laser_scan_amplitude(laser.rampAmplitudeHSW);
    set_ad5693_DC_level(measurement.dCLevelW);
    
    VLD_ON();  

    set_ad5693_laser_bias_current(laser.currentHSW);
    //set_laser_scan_amplitude(laser.rampAmplitudeHSW);
    //set_trigger_level(laser.triggerLevelHSW);
    set_ad5693_DC_level(measurement.dCLevelW);
		AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, measurement.amplResistorValue, 0);
		AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, measurement.amplResistorValue, 0);
    laser.laserStatus=ON;
    //LED2_ON();
    timCiclo1ms = Tc_housekeeping_state;
    ptrTaskManager = warming_housekeeping_state;
}


void measurement_state(void){
    realTime.laserState= 40;
    timCiclo1ms = Tc_test_state;
    //LED4_ON();
    read_rampa_DC(&service.adcValueOriginal[0],measurement.nAcquisitionPointsW,measurement.nAvgW);
	if (counterM<76){
			counterM++;
			if (counterM==76)
				measurement.ready=1;
		}

		calculate_concentration_diff();
    set_sensitivity_level();
    if (measurement.centerFlag==ON && counterM>20)
			line_center();
//    if (realTime.concValue>10)
//        measurement.centerFlag=ON;
    timCiclo1ms = Tc_measurement;
    ptrTaskManager = change_settings_state;
    if (RoadRunner.errorFlag==ON && (laser.voltageR<0.5f || laser.voltageR>2.5f)){
      ptrTaskManager = error_state;
        realTime.errorType=5;
    }
    if (RoadRunner.errorFlag==ON && (laser.currentLaserR<0.001f)){
      ptrTaskManager = error_state;
        realTime.errorType=6;
    }      
}

void measurement_housekeeping_state(void){
    realTime.laserState=50;
    read_laser_temperature();
    read_laser_voltage();
    read_laser_current();
    read_peltier_current();
    read_cell_temperature();
    timCiclo1ms = Tc_measurement_state;
    ptrTaskManager = communication_state;
}

void communication_state(void){
		static uint8_t indUart=0;
		uint32_t uwCRCValueCheck;
  	volatile uint8_t ptrTxBufData[256];
//		char ptrRxBufData[256];

    if (realTime.laserState<100 && service.printFLag==ON){
      if (service.nV==0){
        print_n_values(&service.adcValueOriginal[0], measurement.nAcquisitionPointsW);
				//HAL_SPI_TransmitReceive_DMA(&hspi1, (uint8_t *)toCPU_TxData, (uint8_t *)fromCPU_RxData, SIZEBUFRXTX);
      }else if (service.nV==1){
				print_n_values(&service.adcValueElaborated[0], measurement.nAcquisitionPointsW);  
				//HAL_SPI_TransmitReceive_DMA(&hspi1, (uint8_t *)toCPU_TxData, (uint8_t *)fromCPU_RxData, SIZEBUFRXTX);
      }else if (service.nV==2)
			print_conc();//
			else if (service.nV==4)
				print_n_values_2(&service.adcValueOriginal[0], realTime.nprint);
			else if (service.nV==5)
				print_n_values_2(&service.adcValueElaborated[0], realTime.nprint);
				//print_n_values_3(&service.adcValueOriginal[0], realTime.nprint);
			else{
				sprintf( (char *)RoadRunner.tmpBuf, "a\r\n");

			}
			sprintf((char *)ptrTxBufData, "%2d ; %3.1f ; %2d ; %d ",indUart, realTime.concValue,realTime.laserState,realTime.sensitivityLevel);	
			uwCRCValueCheck = HAL_CRC_Calculate(&hcrc,(uint32_t *)  ptrTxBufData , strlen((char*)ptrTxBufData));	
			sprintf((char *)ptrTxBufData, "%s%04X\n",ptrTxBufData,uwCRCValueCheck&0xFFFF);
			HAL_UART_Transmit (&huart1, (uint8_t *)ptrTxBufData, strlen((char *)ptrTxBufData), 100);
			//pippo=HAL_UART_Receive(&huart1, (uint8_t *)ptrRxBufData, 5, 1000);
			//if (strchr(ptrRxBufData,':')!=NULL){
			//		sprintf((char *)ptrTxBufData, "%2d ; 0.4 ; %2d ",indUart,realTime.laserState);	
			//}
			indUart++;
			if (indUart>9)
				indUart=0;

    } 
		B2B_USART_EnableCPU_TX_Laser_RX();
    timCiclo1ms = Tc_dummy_state;
    ptrTaskManager=measurement_state;
}

void print_conc(){
    sprintf( (char *)RoadRunner.tmpBuf, "{\r\n %3.6f \r\n %3.6f \r\n %4.1f \r\n %4.1f \r\n}\r\n",realTime.concValue,realTime.concValueRel,realTime.power,realTime.bit);
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
}
void print_n_values(uint32_t *pvalues, uint16_t n_values){
    sprintf( (char *)RoadRunner.tmpBuf, "{\r\n");
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%3.6f\r\n",realTime.concValue );
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%3.6f\r\n",realTime.concValueRel );
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%d\r\n",realTime.sensitivityLevel);		
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%4.1f\r\n",realTime.power);			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%4.1f\r\n",realTime.bit);			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%2.2f\r\n",laser.voltageR );			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%d\r\n",service.nV);			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );    
    for (uint16_t i=0; i<n_values; i++){
			sprintf( (char *)RoadRunner.tmpBuf, "%d\r\n", *(pvalues+i));			
			sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
		}
    sprintf( (char *)RoadRunner.tmpBuf, "}\r\n");			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );  
}

void print_n_values_2(uint32_t *pvalues, uint16_t n_values){
    sprintf( (char *)RoadRunner.tmpBuf, "{");
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%3.6f;",realTime.concValue );
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%3.6f;",realTime.concValueRel );
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%d;",realTime.sensitivityLevel);		
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%4.1f;",realTime.power);			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "%2.1f",realTime.bit);					
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	  sprintf( (char *)RoadRunner.tmpBuf, " | ");			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );  
    for (uint16_t i=0; i<n_values-1; i++){
			sprintf( (char *)RoadRunner.tmpBuf, "%d;", *(pvalues+i));			
			sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
		}
		sprintf( (char *)RoadRunner.tmpBuf, "%d", *(pvalues+n_values-1));			
		sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	  sprintf( (char *)RoadRunner.tmpBuf, " | ");			
		sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    for (uint16_t i=0; i<130; i++){
			sprintf( (char *)RoadRunner.tmpBuf, "%d;", service.adcValueElaborated[i]);			
			sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
		}
		sprintf( (char *)RoadRunner.tmpBuf, "XX");			
		sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
    sprintf( (char *)RoadRunner.tmpBuf, "}\r\n");			
    sendUsart2( (uint8_t *)RoadRunner.tmpBuf );  
}
void print_n_values_3(uint32_t *pvalues, uint16_t n_values){
		char printBuff[3700];
    sprintf( printBuff, "{%3.6f;%3.6f;%d;%4.1f;%2.1f|",realTime.concValue,realTime.concValueRel,realTime.sensitivityLevel,realTime.power,realTime.bit);
    //sendUsart2((uint8_t *)printBuff);
    for (uint16_t i=0; i<n_values-1; i++){
			sprintf( printBuff+strlen(printBuff), "%d;", *(pvalues+i));			
			//sendUsart2((uint8_t *) printBuff);
		}
		sprintf( printBuff+strlen(printBuff), "%d;XX}\r\n", *(pvalues+n_values-1));				
    sendUsart2((uint8_t *)printBuff);  
}


void dummy_state(void){
  if ( timCiclo1ms )         
        return;
	B2B_USART_DisableCPU_TX_Laser_RX();
  timCiclo1ms = Tc_measurement_state;
  ptrTaskManager=measurement_state;
}

void calculate_concentration_diff(){
  uint16_t xmin;
  uint32_t ymin1;
  uint32_t ymin2;
  uint16_t xmax;
  uint32_t ymax;
	uint8_t sep=analysis.sep;
	//float conc;
	float y_mean1;
	float y_mean2;
	int16_t diffmin;
	uint16_t startInd, rngInd;
	startInd=52;rngInd=30;
	float normF=1000000.0f;
	//smooth a 10 punti
	for( uint16_t i=0; i<measurement.nAcquisitionPointsW; i++){
		service.adcValueElaborated[i]=service.adcValueOriginal[i];
	}
	minmaxmeanV(&(service.adcValueElaborated[0]),analysis.middle_point, &y_mean1, &xmin, &ymin1,&xmax, &ymax);
	realTime.power=y_mean1/((float)measurement.nAvgW);//-measurement.dCLevelW*measurement.convertionFactorW;
	realTime.bit=log2f((ymax-ymin1)/((float)measurement.nAvgW));
	service.adcValueElaborated[0]=service.adcValueElaborated[1];
	smooth(analysis.nsm, service.adcValueElaborated, analysis.middle_point);
	smooth(analysis.nsm, &(service.adcValueElaborated[analysis.middle_point]), analysis.middle_point);
	meanv(&(service.adcValueOriginal[0]), &y_mean1, analysis.middle_point);
	meanv(&(service.adcValueOriginal[0]), &y_mean2, analysis.middle_point);
//	realTime.concValue=y_mean;
	for( uint16_t i=0; i<analysis.middle_point; i++){
		service.adcValueElaborated[i]=service.adcValueElaborated[i]*(normF/y_mean1)+0.5f;
		service.adcValueElaborated[i+analysis.middle_point]=service.adcValueElaborated[i+analysis.middle_point]*(normF/y_mean2)+0.5f;
	}
	for (uint16_t i=0;i<analysis.middle_point-2*sep; i++){
		service.adcValueElaborated[i]=normF+service.adcValueElaborated[i+sep]-(service.adcValueElaborated[i]+service.adcValueElaborated[i+2*sep])/2.0f;
		service.adcValueElaborated[i+analysis.middle_point]=normF+service.adcValueElaborated[i+sep+analysis.middle_point]-
															(service.adcValueElaborated[i+analysis.middle_point]+service.adcValueElaborated[i+2*sep+analysis.middle_point])/2.0f;
	}
	for( uint16_t i=0; i<analysis.middle_point; i++){
		service.adcValueFinal[i]=(service.adcValueElaborated[i]+service.adcValueElaborated[i+analysis.middle_point])/2;
	}
	minmaxV(&(service.adcValueElaborated[startInd]),rngInd, &xmin, &ymin1,&xmax, &ymax);
	realTime.concValue=((service.adcValueElaborated[startInd+xmin-sep]+service.adcValueElaborated[startInd+xmin+sep])/2.0f-ymin1)/100.0f;
	minmaxV(&(service.adcValueElaborated[startInd+analysis.middle_point]),10, &xmin, &ymin2,&xmax, &ymax);
	realTime.concValueRel=((service.adcValueElaborated[startInd+xmin-sep+analysis.middle_point]+service.adcValueElaborated[startInd+xmin+sep+analysis.middle_point])/2.0-ymin2)/100.0f;
	realTime.concValue=(realTime.concValue+realTime.concValueRel)/2.0f;
	minmaxV(&(service.adcValueFinal[startInd+analysis.middle_point]),rngInd, &xmin, &ymin2,&xmax, &ymax);
	realTime.concMean=((service.adcValueFinal[startInd+xmin-sep]+service.adcValueFinal[65+xmin+sep])/2.0f-ymin1)/100.0f;
	
	if (realTime.sensitivityLevel==LOW){
		realTime.concValue*=analysis.calibLow;
		realTime.concMean*=analysis.calibLow;
		realTime.concValueRel*=analysis.calibLow;
	}else{
	 	realTime.concValue*=analysis.calibHigh;
	 	realTime.concMean*=analysis.calibHigh;
		realTime.concValueRel*=analysis.calibHigh;;
  }
	if(laser.ID==0x0008)
		realTime.concValue-=13;
	if(realTime.concValue<0.0f)
		realTime.concValue=0;
}



void change_settings_state(void){
    realTime.laserState=70;
    if (measurement.nAcquisitionPointsW!=nSamplesDef){
        measurement.nAcquisitionPointsW=nSamplesDef;
    }
    if (laser.temperatureW!=tLasDef){
      set_ad5693_laser_temperature(laser.temperatureW);
      tLasDef=laser.temperatureW;
    }
    if (laser.currentHSW!=iLasHSDef){
      set_ad5693_laser_bias_current(laser.currentHSW);
      iLasHSDef=laser.currentHSW;
    }
    if (laser.rampAmplitudeHSW!=iAMPHSDef){
      set_laser_scan_amplitude(laser.rampAmplitudeHSW);
			for (uint16_t it=0;it<1024;it++){
				service.waveform[it] = 1.225/3.0*4096.0-(wave_point_table[it])*(1672.0/4096.0)*laser.rampAmplitudeHSW/2.6;
			}
      iAMPHSDef=laser.rampAmplitudeHSW;
//			htim6.Init.Period = 690;
			//MX_TIM6_Init();
			//HAL_TIM_Base_Start(&htim6); 
    }                   
    if (laser.triggerLevelHSW!=lTriggDef){
//      set_trigger_level(laser.triggerLevelHSW);
      lTriggDef=laser.triggerLevelHSW;
    }
     if (measurement.nAvgW!=nMeanDef){
        nMeanDef=measurement.nAvgW;
    }
     if (laser.rampFrequencyW!=iScanFreqDef){
        //start_laser_scan(laser.rampFrequencyW);
        iScanFreqDef=laser.rampFrequencyW;
    }
    if (measurement.dCLevelW!=lDCDef){
        set_ad5693_DC_level(measurement.dCLevelW);
        lDCDef=measurement.dCLevelW;
    }
		if (measurement.amplResistorValue!=amplResistorValueDef){
			AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, measurement.amplResistorValue, 0);
//			HAL_Delay(100);
			AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, measurement.amplResistorValue, 0);
			amplResistorValueDef=measurement.amplResistorValue;
    }
    ptrTaskManager = measurement_housekeeping_state;


}


void minV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin){
  *xmin=0;*ymin=*v;
  for (uint16_t i=1; i<dim; i++)
  {
    if (*ymin>*(v+i)){
        *ymin=*(v+i);
        *xmin=i;
    }
  }
}

void meanv(uint32_t *v,	float *y_mean, uint16_t dim){
	*y_mean=0.0;
	for (uint16_t i=0; i<dim; i++){
		*y_mean+=*(v+i);	
	}	
	*y_mean/=dim;
}


void minmaxmeanV(uint32_t *v,uint16_t dim,float *y_mean, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax){
  *xmin=0;*ymin=*v;
	*xmax=0;*ymax=*v;
	*y_mean=0.0;
  for (uint16_t i=1; i<dim; i++)
  {
    if (*ymin>*(v+i)){
        *ymin=*(v+i);
        *xmin=i;
    }
		if (*ymax<*(v+i)){
        *ymax=*(v+i);
        *xmax=i;
    }
		*y_mean+=*(v+i);	
  }
	*y_mean/=dim;
}

void minmaxV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax){
  *xmin=0;*ymin=*v;
	*xmax=0;*ymax=*v;
  for (uint16_t i=1; i<dim; i++)
  {
    if (*ymin>*(v+i)){
        *ymin=*(v+i);
        *xmin=i;
    }
		if (*ymax<*(v+i)){
        *ymax=*(v+i);
        *xmax=i;
    }
  }
}



void smooth(uint8_t nsm, uint32_t *x, uint16_t dim){
  float v[1000];
	uint8_t nsmtmp;

  for (uint16_t i=0; i<dim;i++){
		*(v+i)=*(x+i);
		nsmtmp=nsm;
		if (i<nsm)
			nsmtmp=i;
		if (dim-1-i<nsm)
				nsmtmp=dim-1-i;
		for (uint8_t j=0; j<nsmtmp; j++){
			*(v+i)+=*(x+i+j+1)+*(x+i-j-1);
		}
    *(v+i)/=(2.0f*nsmtmp+1.0f);
  }
  for (uint16_t i=0; i<dim;i++)
    *(x+i)=(uint32_t)(*(v+i)+0.5f);
  
}


void line_center(){

  int16_t diff;
  uint16_t xmin;
  uint32_t ymin1;
  uint32_t ymin2;
  uint16_t xmax;
  uint32_t ymax;
  uint16_t center;
  
  center= (analysis.middle_point-2*analysis.sep)/2;
  
  minmaxV(&(service.adcValueElaborated[center-30]),60, &xmin, &ymin1,&xmax, &ymax);
  xmin+=center-30;
  diff=xmin-center;
  if (abs(diff)>3 && realTime.sensitivityLevel==HIGH && realTime.concValue>1.5f  && measurement.centerFlag==ON && ((laser.currentHSW+diff*0.001f)<iLasHSDef+0.1f && (laser.currentHSW+diff*0.001f)>iLasHSDef-0.1f)){
    laser.currentHSW=laser.currentHSW-diff*0.001f;
    set_ad5693_laser_bias_current(laser.currentHSW);
  }
  if (abs(diff) && realTime.sensitivityLevel==LOW && realTime.concValue<500000 && measurement.centerFlag==ON && ((laser.currentLSW+diff*0.001f)<iLasLSDef+0.1f && (laser.currentLSW+diff*0.001f)>iLasLSDef-0.1f)){
    laser.currentLSW=laser.currentLSW-diff*0.001f;
    set_ad5693_laser_bias_current(laser.currentLSW);
  }
}




void taskSPI_communication(void)
{
    realTime.timeStamp=epochTimeStamp_now(&hrtc);
    if (measurement.ready==1)
      realTime.laserState=41;
    sprintf (RoadRunner.tmpBuf ,"*%uld %f %f %u\r\n",realTime.timeStamp, realTime.concValue, realTime.concValueRel, realTime.laserState); 
    HAL_UART_Transmit ( &huart1, ( uint8_t * ) RoadRunner.tmpBuf ,strlen ( RoadRunner.tmpBuf ),1000 );
    if (realTime.laserState>100)
      ptrTaskManager = error_state;
    else
      ptrTaskManager = measurement_housekeeping_state;
//      ptrTaskManager = change_settings_state;
}

void write_laser_parameters(void){
	uint16_t laserID=12;
	writeLong(EEP_ID, 1 );
	writeLong(EEP_LASER_ID, laserID );
	if(laserID==1){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 6.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.5f );
		writeFloat(EEP_LASER_CURRENT_LS, 6.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 0.5f );
	}else if(laserID==2){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 6.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.5f );
		writeFloat(EEP_LASER_CURRENT_LS, 6.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 0.5f );
	}else if(laserID==3){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 6.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.5f );
		writeFloat(EEP_LASER_CURRENT_LS, 6.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 0.5f );
	}else if(laserID==4){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 7.44f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.4f );
		writeFloat(EEP_LASER_CURRENT_LS, 5.9f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 1.0f );
		writeFloat(EEP_CALIB_HIGH, 0.6f );		
		writeFloat(EEP_CALIB_LOW, 75.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else if(laserID==5){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 8.2f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.9f );
		writeFloat(EEP_LASER_CURRENT_LS, 5.65f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 0.8f );
		writeFloat(EEP_CALIB_HIGH, 0.3f );		
		writeFloat(EEP_CALIB_LOW, 63.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else if(laserID==6){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 7.77f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.8f );
		writeFloat(EEP_LASER_CURRENT_LS, 5.65f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 0.8f );
		writeFloat(EEP_CALIB_HIGH, 0.7f );		
		writeFloat(EEP_CALIB_LOW, 63.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else if(laserID==7){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 6.3f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.7f );
		writeFloat(EEP_LASER_CURRENT_LS, 4.95f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 1.0f );
		writeFloat(EEP_CALIB_HIGH, 1.4f );		
		writeFloat(EEP_CALIB_LOW, 143.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 36 );
	}else if(laserID==8){
		writeFloat(EEP_LASER_TEMPERATURE, 22.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 7.27f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 1.0f );
		writeFloat(EEP_LASER_CURRENT_LS, 5.9f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 1.0f );
		writeFloat(EEP_CALIB_HIGH, 0.6f );		
		writeFloat(EEP_CALIB_LOW, 75.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else if(laserID==11){
		writeFloat(EEP_LASER_TEMPERATURE, 24.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 6.5f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 1.0f );
		writeFloat(EEP_LASER_CURRENT_LS, 5.15f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 1.0f );
		writeFloat(EEP_CALIB_HIGH, 0.12f );		
		writeFloat(EEP_CALIB_LOW, 27.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else if(laserID==12){
		writeFloat(EEP_LASER_TEMPERATURE, 24.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 5.7f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 1.0f );
		writeFloat(EEP_LASER_CURRENT_LS, 3.8f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 1.0f );
		writeFloat(EEP_CALIB_HIGH, 0.6f );		
		writeFloat(EEP_CALIB_LOW, 75.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else if(laserID==13){
		writeFloat(EEP_LASER_TEMPERATURE, 24.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 5.8f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 1.0f );
		writeFloat(EEP_LASER_CURRENT_LS, 4.0f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 1.0f );
		writeFloat(EEP_CALIB_HIGH, 0.61f );		
		writeFloat(EEP_CALIB_LOW, 78.0f );
		writeFloat(EEP_CONVERSION_FACTOR, 20 );
	}else{
		writeFloat(EEP_LASER_TEMPERATURE, 25.0f );
		writeFloat(EEP_LASER_CURRENT_HS, 5.0f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, 0.5f );
		writeFloat(EEP_LASER_CURRENT_LS, 5.1f );
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, 0.5f );
	}
	writeFloat(EEP_LASER_RAMP_FREQUENCY, 200 );
	writeWord(EEP_NUMBER_OF_ACQUISITION, 200);
}
void load_struct(void){
//	if (1){

//		////////////laser IIIb
////		laser.currentHSW=6.35;		// a -- laser.currentHSW=7.62;//b  --  laser.currentHSW=7.3; -- c
////		laser.temperatureW=22;	// a -- laser.temperatureW=21;//b -- laser.temperatureW=25;  -- c
////		laser.rampAmplitudeHSW=0.7;		//laser.rampAmplitudeHSW=2.6;		 
////		////////////laser IIIb
////		laser.currentHSW=7.28;		// a -- laser.currentHSW=7.62;//b  --  laser.currentHSW=7.3; -- c
////		laser.temperatureW=22;	// a -- laser.temperatureW=21;//b -- laser.temperatureW=25;  -- c
////		laser.rampAmplitudeHSW=1;		//laser.rampAmplitudeHSW=2.6;		

		//writeFloat ( EEP_ID, 0.2 );
//		sprintf ( RoadRunner.tmpBuf,"[EEP_ID] = %f\r\n", readFloat ( EEP_ID ));
//		HAL_UART_Transmit ( &huart1, ( uint8_t * ) RoadRunner.tmpBuf,strlen ( RoadRunner.tmpBuf ),1000 );

//		incFloat(EEP_ID);
//    
//		sprintf ( RoadRunner.tmpBuf,"[EEP_ID] = %f\r\n", readFloat ( EEP_ID ));
//		HAL_UART_Transmit ( &huart1, ( uint8_t * ) RoadRunner.tmpBuf,strlen ( RoadRunner.tmpBuf ),1000 );	
//		sprintf ( RoadRunner.tmpBuf,"[EEP_ID] = %f\r\n", readFloat ( EEP_ID ));
//		HAL_UART_Transmit ( &huart1, ( uint8_t * ) RoadRunner.tmpBuf,strlen ( RoadRunner.tmpBuf ),1000 );

//		incFloat(EEP_ID);
//    
//		sprintf ( RoadRunner.tmpBuf,"[EEP_ID] = %f\r\n", readFloat ( EEP_ID ));
//		HAL_UART_Transmit ( &huart1, ( uint8_t * ) RoadRunner.tmpBuf,strlen ( RoadRunner.tmpBuf ),1000 );	
//		incFloat(EEP_ID);
//    
//		sprintf ( RoadRunner.tmpBuf,"[EEP_ID] = %f\r\n", readFloat ( EEP_ID ));
//		HAL_UART_Transmit ( &huart1, ( uint8_t * ) RoadRunner.tmpBuf,strlen ( RoadRunner.tmpBuf ),1000 );	
//		
		write_laser_parameters();
		laser.ID = readLong(EEP_LASER_ID);
		if(laser.ID !=laser.ID ){
			laser.ID =8;
		}
		laser.temperatureW = readFloat(EEP_LASER_TEMPERATURE);
		if(laser.temperatureW!=laser.temperatureW){
			laser.temperatureW=23.0f;
		}
		laser.currentHSW = readFloat(EEP_LASER_CURRENT_HS);
		if(laser.currentHSW!=laser.currentHSW){
			laser.currentHSW=7.28f;
		}
		laser.rampAmplitudeHSW = readFloat(EEP_LASER_RAMP_AMPLITUDE_HS);
		if(laser.rampAmplitudeHSW!=laser.rampAmplitudeHSW){
			laser.rampAmplitudeHSW=1.0f;
		}
		laser.rampFrequencyW = readFloat(EEP_LASER_RAMP_FREQUENCY);
		if(laser.rampFrequencyW!=laser.rampFrequencyW){
			laser.rampFrequencyW=200.0f;
		}
		laser.currentLSW = readFloat(EEP_LASER_CURRENT_LS);
		if(laser.currentLSW !=laser.currentLSW ){
			laser.currentLSW =7.0f;
		}
		laser.rampAmplitudeLSW = readFloat(EEP_LASER_RAMP_AMPLITUDE_LS);
		if(laser.rampAmplitudeLSW!=laser.rampAmplitudeLSW){
			laser.rampAmplitudeLSW=1.0f;
		}
		analysis.calibHigh = readFloat(EEP_CALIB_HIGH);
		if(analysis.calibHigh !=analysis.calibHigh){
			analysis.calibHigh =1.5f;
		}
		analysis.calibLow = readFloat(EEP_CALIB_LOW);
		if(analysis.calibLow !=analysis.calibLow){
			analysis.calibLow =1.5f;
		}
		measurement.convertionFactorW = readFloat(EEP_CONVERSION_FACTOR);
		if(measurement.convertionFactorW !=measurement.convertionFactorW){
			measurement.convertionFactorW =36.0f;
		}
	
	//laser.currentHSW=6.3;		// a -- laser.currentHSW=7.62;//b  --  laser.currentHSW=7.3; -- c
	//laser.temperatureW=22;	// a -- laser.temperatureW=21;//b -- laser.temperatureW=25;  -- c
	//laser.rampAmplitudeHSW=0.7;		//laser.rampAmplitudeHSW=2.6;		 
  analysis.start_point=50; analysis.end_point=50;  analysis.middle_point=225; 
  analysis.nsm=5; analysis.semi_area_point=40; analysis.sep=45;
  laser.laserStatus=OFF; laser.tecStatus=OFF;
  measurement.nAcquisitionPointsW=450;    measurement.nAvgW=55;
  measurement.dCLevelW=32768;
	//measurement.convertionFactorW=36;//8= (68Kohm/1kohm)*2*1.225/(2.5*1.225) 

  realTime.concValue=0.0;
  realTime.concValueRel=0.0;
  service.nStab=0;
  service.nV=0; service.printFLag=ON;
	realTime.nprint=measurement.nAcquisitionPointsW;
}

void init_BLE_UART(void){
	sprintf( (char *)RoadRunner.tmpBuf, "ATZ\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)RoadRunner.tmpBuf, "AT+FACTORYRESET\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)RoadRunner.tmpBuf, "AT+GAPDEVNAME=SENSIT PMD2\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)RoadRunner.tmpBuf, "AT+GATTADDSERVICE=UUID=0x180D\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)RoadRunner.tmpBuf, "AT+GATTADDCHAR=UUID=0x2A37, PROPERTIES=0x10, MIN_LEN=2, MAX_LEN=3, VALUE=00-40\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)RoadRunner.tmpBuf, "AT+GATTADDCHAR=UUID=0x2A38, PROPERTIES=0x02, MIN_LEN=1, VALUE=3\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)RoadRunner.tmpBuf, "AT+GAPSETADVDATA=02-01-06-05-02-0d-18-0a-18\r");
	HAL_Delay(1000);
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	sprintf( (char *)RoadRunner.tmpBuf, "ATZ\r");
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
	HAL_Delay(1000);
}

void print_BLE_UART(void){
	sprintf( (char *)RoadRunner.tmpBuf, "AT+GATTCHAR=1,00-%x\r",(uint32_t)realTime.concValue);
  sendUsart2( (uint8_t *)RoadRunner.tmpBuf );
}