/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "i2c_dac_AD5693.h"

/* Private variables ---------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c2;




uint8_t readbackBuffer[8];


//HAL_StatusTypeDef ad5693_is_ready(void)
//{
//    return HAL_I2C_IsDeviceReady(&hi2c2, AD5693_ADDR, TRIAL_COUNT, COMM_TIMEOUT);
//}

HAL_StatusTypeDef ad5693_is_ready(I2C_HandleTypeDef *phi2c, uint16_t DevAddress)
{
    return HAL_I2C_IsDeviceReady(phi2c, DevAddress, TRIAL_COUNT, COMM_TIMEOUT);
}


/**
  * @brief  The application entry point.
  *
  * @retval None
  */
HAL_StatusTypeDef ad5693_set_channel(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, uint16_t value)
{
  uint8_t buffer[3];
  HAL_StatusTypeDef tmpRetVal;

  if ( (tmpRetVal=ad5693_is_ready(phi2c, DevAddress)) == HAL_OK ) {

    buffer[0]=AD5693_WRITE_DAC_AND_INP_REG;
    
    buffer[1]=(value>>8) & 0xFF;        //MSB byte  
    buffer[2]= value & 0xFF;            //LSB byte  
    tmpRetVal=HAL_I2C_Master_Transmit(phi2c, DevAddress,buffer,3,100);  

 }
  
  return tmpRetVal;
}


HAL_StatusTypeDef ad5693_read_back(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, uint8_t *buf_read_back)
{
  uint8_t buffer;
  HAL_StatusTypeDef tmpRetVal;

  if ( (tmpRetVal=ad5693_is_ready(phi2c, DevAddress)) == HAL_OK ) {

    // ReadBack
    buffer=AD5693_WRITE_DAC_AND_INP_REG;
    
    HAL_I2C_Master_Transmit(phi2c, DevAddress, &buffer, 1, 100);
    HAL_I2C_Master_Receive(phi2c, DevAddress, buf_read_back, 2, 100);

 }
  
  return tmpRetVal;
}


