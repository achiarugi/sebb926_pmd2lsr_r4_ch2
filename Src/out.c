#include "stm32l4xx_hal.h"
#include "out.h"
#include "main.h"

/*
 *
 */

//void LED1_ON(){
//    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
//}

//void LED1_OFF(){
//     HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
//}


/*
 *
 */
  
void VLD_ON(){
   HAL_GPIO_WritePin(ON_VLD_GPIO_Port, ON_VLD_Pin, GPIO_PIN_SET);
}

void VLD_OFF(){
   HAL_GPIO_WritePin(ON_VLD_GPIO_Port, ON_VLD_Pin, GPIO_PIN_RESET);
}

void TC_ON(){
   HAL_GPIO_WritePin(ON_TC_GPIO_Port, ON_TC_Pin, GPIO_PIN_SET);
}

void TC_OFF(){
   HAL_GPIO_WritePin(ON_TC_GPIO_Port, ON_TC_Pin, GPIO_PIN_RESET);
}

/**
  * @brief  Reset function external watch-dog MAX823 device with WDI line
  *         PAY ATTENTION: minimum Watchdog Timeout Period 1.12s
  *
  * @param  None
  * @retval None
  */
void WDI_TOGGLE() {
   HAL_GPIO_TogglePin(WDI_GPIO_Port, WDI_Pin);
}
