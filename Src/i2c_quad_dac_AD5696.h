#ifndef __I2C_QUAD_DAC_AD5696_H
#define __I2C_QUAD_DAC_AD5696_H

#define TRIAL_COUNT     (3)
#define COMM_TIMEOUT    (1000)
#define AD5696_ADDR     (0x0C << 1)

typedef enum {
	NO_OPERATION                    = 0x00,
	WRITE_TO_INPUT_REGISTER_N       = 0x10,
	UPDT_DAC_REG_N_WITH_INP_REG_N	= 0x20,
	WRITE_TO_AND_UPDT_DAC_N 	= 0x30,
        PWR_DOWN_PWR_UP                 = 0x40,
        HW_LDAC_MSK_REG                 = 0x50,
        SW_RESET                        = 0x60,
} AD5696_Command_Bits;

#define AD5696_DAC_A                    0x01
#define AD5696_DAC_B                    0x02
#define AD5696_DAC_C                    0x04
#define AD5696_DAC_D                    0x08

HAL_StatusTypeDef ad5696_is_ready(void);
HAL_StatusTypeDef ad5696_set_channel(uint16_t channel, uint16_t value);
HAL_StatusTypeDef ad5696_read_back(uint8_t *buf_read_back);

#endif

