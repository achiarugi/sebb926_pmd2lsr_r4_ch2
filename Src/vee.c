#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "main.h"
#include "stm32l4xx_hal.h"
#include "vee.h"
#include "ad.h"

#define         MAX_SCAN_AMPLITUDE                      2
#define         MAX_LASER_CURRENT       					      11.0f
#define         A_COEFF                                 0.00121553f
#define         B_COEFF                                 0.0002191519f
#define         C_COEFF                                 0.0000001524069f
#define         R0                                      10000

//uint8_t set_laser_scan_amplitude(float amp){
//  uint16_t ampDAC;
//  ampDAC=(uint16_t)(amp/MAX_SCAN_AMPLITUDE*32768.0f);
//  if (ampDAC>32768)
//    ampDAC=32768;
//  ampDAC=32768-ampDAC;
//  ad5696_set_channel(WRITE_TO_AND_UPDT_DAC_N |AD5696_DAC_A, ampDAC);
//  return 0;
//}
//uint8_t set_laser_bias_current(float iLas){
//  uint16_t iLasDAC;
//  iLasDAC=(uint16_t)(iLas/MAX_LASER_CURRENT*32768.0f);
//  if (iLasDAC>32768)
//    iLasDAC=32768;
//  iLasDAC=32768-iLasDAC;
//  ad5696_set_channel(WRITE_TO_AND_UPDT_DAC_N |AD5696_DAC_B, iLasDAC);
//  return 0;
//}

//uint8_t set_trigger_level(uint16_t lTriggDAC){
//  ad5696_set_channel(WRITE_TO_AND_UPDT_DAC_N |AD5696_DAC_C, lTriggDAC);
//  return 0;
//}

uint8_t set_laser_scan_amplitude(float amp){
	for (uint16_t it=0;it<1024;it++){
			service.waveform[it] = 1.225/3.0*4096.0-(wave_point_table[it])*(1672.0/4096.0)*amp/2.6;
	}
	return 0;
}

uint8_t set_ad5693_DC_level(uint16_t vDC){
  ad5696_set_channel(WRITE_TO_AND_UPDT_DAC_N |AD5696_DAC_D, vDC);
	ad5693_set_channel(&hi2c3, AD5693_ADDR_DCLEVEL, vDC);
  return 0;
}

uint8_t set_ad5693_laser_temperature(float tLas){
  uint16_t tLasDAC;
  tLasDAC=convertLaserTemperature(tLas);
  ad5693_set_channel(&hi2c2, AD5693_ADDR_TEMPLAS, tLasDAC);
  return 0;
}

uint8_t set_ad5693_laser_bias_current(float iLas){
  uint16_t iLasDAC;
  iLasDAC=(uint16_t)(iLas/MAX_LASER_CURRENT*65535.0f);
  if (iLasDAC>65535)
    iLasDAC=65535;
  iLasDAC=65535-iLasDAC;
	ad5693_set_channel(&hi2c3, AD5693_ADDR_ILAS, iLasDAC);
  return 0;
}



uint8_t read_laser_temperature(void){
  float tTemp;
  tTemp=(float)avg_ad_TEMP_LASER;
  tTemp=tTemp*2/4095;
  tTemp=10000*tTemp/(1-tTemp);
//  laser.temperatureR=1.0/(A_COEFF+B_COEFF*log(tTemp)+C_COEFF*pow(log(tTemp),3))-273.15;
  return 0;
}
uint8_t read_laser_voltage(void){
  laser.voltageR=3.0/4095.0*((float)(avg_ad_LASER_ANODE_BUF-avg_ad_LASER_CATHODE_BUF));
  return 0;
}
uint8_t read_laser_current(void){
  laser.currentLaserR=3.0/4095.0/33.0*((float)(avg_ad_LASER_CATHODE_BUF));
  return 0;
}
uint8_t read_peltier_current(void){
  laser.currentTecR=(3.0/4095*((float)avg_ad_IPELT)-1.5)/(8*0.068);
  return 0;
}
uint8_t read_cell_temperature(void){
  laser.temperatureCellR=0;
  return 0;
}

uint16_t convertLaserTemperature(float tLas){
  uint16_t tLasConv;
  float x, y, tTemp;
  x=1.0f/C_COEFF*(A_COEFF-1.0f/(tLas+273.15f))/2.0f;
  y=sqrt(pow(B_COEFF/3/C_COEFF,3)+x*x);
  tTemp=exp(pow(y-x,1.0/3.0)-pow(y+x,1.0/3.0));
  tLasConv=(uint16_t)(tTemp/(R0+tTemp)*65535);
  return tLasConv;
}
