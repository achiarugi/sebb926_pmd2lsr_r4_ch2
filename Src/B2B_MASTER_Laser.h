#ifndef B2B_COMMUNICATION_H
#define B2B_COMMUNICATION_H

#include <stdint.h>

#define DIM_USART1_RxBuffer 32

void B2B_USART_MAIN_Laser_CPU(void);
void B2B_USART_LaserReceive_completeTXRX (void) ;
void B2B_USART_EnableCPU_TX_Laser_RX(void);
void B2B_USART_DisableCPU_TX_Laser_RX(void);
#endif
