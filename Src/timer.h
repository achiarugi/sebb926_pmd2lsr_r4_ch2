#ifndef INITTIMER_H
#define INITTIMER_H


void sleep ( unsigned int numMsec );
void TB2_int ( void );
extern unsigned int timCiclo1ms;
extern unsigned int timSleeping1ms;
extern unsigned int timCiclo1msBUZZER;
extern unsigned int timCiclo1modulazioneBUZZER;
extern unsigned int timMenu1ms;
extern unsigned int timCiclo1000ms;
extern unsigned int timMainCiclo1ms;
extern unsigned int timRTCread1ms;
extern unsigned long int timCicloTest_1ms;
extern unsigned long int tim1ms_testDuration;
extern unsigned int i2c_1ms_Delay;

extern unsigned char flg_ptrTaskManager;
extern unsigned char flg_ptrMenu;
extern unsigned char flg_drawScreen;
extern unsigned char flg_checkGPS;
extern unsigned char flg_disableUBLOXstrings;
extern unsigned char flg_BatteryRtcTemperature;
extern unsigned char flg_readFlussoInput;
extern unsigned char flg_ptrTest;
#endif

