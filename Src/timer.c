#include "timer.h"
#include "sensit.h"

unsigned int timSleeping1ms = 0L;
unsigned int timCiclo1ms = 0L;
unsigned int timMenu1ms=0;
unsigned int timCiclo1msBUZZER = 0;
unsigned int timCiclo1modulazioneBUZZER=0;
unsigned int timCiclo1000ms = 0L;
unsigned int timCiclo1500ms = 0L;
unsigned int timMainCiclo1ms = 0L;
unsigned int timRTCread1ms = 0L;
unsigned long int timCicloTest_1ms = 0L;
unsigned long int tim1ms_testDuration = 0L;
unsigned int i2c_1ms_Delay=0;

unsigned char flg_ptrMenu=TRUE;
unsigned char flg_ptrTaskManager=TRUE;
unsigned char flg_drawScreen=TRUE;
unsigned char flg_checkGPS=TRUE;
unsigned char flg_disableUBLOXstrings=TRUE;
unsigned char flg_BatteryRtcTemperature=TRUE;
unsigned char flg_readFlussoInput=TRUE;
unsigned char flg_ptrTest=TRUE;


void TB2_int ( void );
void TB2_int ( void )
/*------------------------------------------------------------------------*/
/* Description:                                                           */
/*     TimerB2 interrupt                                                  */
/*------------------------------------------------------------------------*/
/* Input:                                                                 */
/* Output:                                                                */
/* After: Increment transmit data                                         */
/**************************************************************************/
{
    // __asm( "FCLR I" );

    //   WD = ~WD;

    if ( timSleeping1ms > 0L )
        --timSleeping1ms;

    if ( timCiclo1ms > 0L )
        --timCiclo1ms;
    
    if(i2c_1ms_Delay>0)
      --i2c_1ms_Delay;

    if ( timMenu1ms > 0L )
        --timMenu1ms;

    if ( timRTCread1ms>0 )
        --timRTCread1ms;

    if ( timMainCiclo1ms % 7 )
        flg_ptrTaskManager=TRUE;

    if ( timMainCiclo1ms % 29 )
        flg_ptrMenu=TRUE;

    timMainCiclo1ms++;

    //---------------------------------------------------------
    // __asm( "FSET I" );  // enable interrupts
}

void sleep ( unsigned int numMsec )
{

    timSleeping1ms = numMsec;
    while ( timSleeping1ms > 0 ) {
        //INTFACE_watchDog();
    }
}


