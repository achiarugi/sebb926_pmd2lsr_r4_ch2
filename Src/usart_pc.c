#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "stm32l4xx_hal.h"
#include "usart_pc.h"
#include "usart.h"
#include "sensit.h"
#include "version.h"
#include "i2C_24AA512.h"
#include "ringbufferdma.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern RingBufferDmaU8 ringPC;
unsigned char ubDataBuffPC[PROTO_BUFFER_SIZE]; /* input/output communication buffer */

unsigned char   checkSumPosPC    = 0x03;
unsigned char   secretCodePC     = 0xB1;
unsigned char   offuscatorPosPC  = 0x01;
unsigned char   productCodePC    = 0xA3;

typeProto statusProtoPC;

unsigned int  offPosPC; /*posizione per offuscatore*/
unsigned char offCharPC;
unsigned char ubInCharPC;
unsigned int ubPosPC;        /* position in buffer (0,1,2,..) */
unsigned int ubLengthPC;     /* length of data in a message used for receiving and transmitting (exclude SOB, include checksum)*/
char          sbCheckSumPC;   /* variable for checksum accumulation */
static        PCP_sResponse sResponsePC;                  /* variable with response data */

#if 0
void usart_PC_init ( void )
{
    statusProtoPC.ST_ST_CHAR_REC  = 0;

    statusProtoPC.STARTED         = 0;
    statusProtoPC.ST_STD_CMD      = 0;
    statusProtoPC.ST_SENDING      = 0;
    statusProtoPC.ST_CS_ADDED     = 0;
    statusProtoPC.ST_ST_SENT      = 0;
}


void removeRXbufferPC()
{
    while ( GetCharUsart_PC ( ( char  * ) &ubInCharPC ) == MA_READY ) {}
}

int usart_PC ( void )
{
    /* Verifico eventuali caratteri ricevuti dal canale seriale */
    while ( GetCharUsart_PC ( ( char  * ) &ubInCharPC ) == MA_READY ) {

       // putCharUsart_PC(ubInCharPC);


            if ( ( statusProtoPC.ST_ST_CHAR_REC ) == 0 ) {               // last byte was not START_CHAR
                if ( ubInCharPC == START_CHAR ) {                           // START_CHAR received
                    statusProtoPC.ST_ST_CHAR_REC = 1;

                    //  pnext0("start");

                } else {                                           // any byte received
                    prepareUsart_PC ( NO_PROTO_START );           // byte received
                }
            } else {                                                 // the last byte was START_CHAR
                if ( ubInCharPC == START_CHAR ) {                           // doubled START_CHAR (this is the second one)
                    prepareUsart_PC ( NO_PROTO_START );           // byte received
                } else {                                           // start of message
                    prepareUsart_PC ( PROTO_START );              // byte received
                }
                statusProtoPC.ST_ST_CHAR_REC = 0;
            }

    } // end while

    return ( FALSE );

}

void prepareUsart_PC ( unsigned int uwStartOfMessage )
{
    if ( uwStartOfMessage == PROTO_START ) {                  /* start of message */

        statusProtoPC.STARTED = 1; /* message receiving */
        sbCheckSumPC = ubDataBuffPC[0] = ubInCharPC;                /* read byte, start of checksum accumulating */
        sbCheckSumPC += secretCodePC;
        ubPosPC = 1;                                               /* next position in buffer */
        ubLengthPC = 4;                                            /* value sufficient for standard format commands */
        if ( ubInCharPC!=productCodePC ) {
            statusProtoPC.STARTED = 0;
            statusProtoPC.ST_ST_CHAR_REC = 0;
            RESPPREPAREPC ( PROTO_STC_DEVICEERR,5 ); /* checksum error response */
            SendResponseUsart_PC ( &sResponsePC );     /* send response to PC */
        }

    } else {                                                      /* no start of message */
        if ( statusProtoPC.STARTED ) {                          /* start of message already detected */

            if ( ubPosPC == 1 ) {
                switch ( ubInCharPC ) {
                case PROTO_CMD_WRITEIMEM_NR:
                case PROTO_CMD_READIMEM:
                case PROTO_CMD_READDMEM:
                case PROTO_CMD_READEMEM:
                case PROTO_CMD_WRITEIMEM:
                case PROTO_CMD_WRITEEMEM:
                    ubLengthPC = 8 + checkSumPosPC;
                    offPosPC=ubLengthPC-offuscatorPosPC;
                    break;
                default:
                    statusProtoPC.STARTED = 0;
                    statusProtoPC.ST_ST_CHAR_REC = 0;
                    RESPPREPAREPC ( PROTO_STC_INVCMD,5 ); /* input buffer overflow */
                    SendResponseUsart_PC ( &sResponsePC );     /* send response to PC */
                    break;
                }
            }

            if ( ubPosPC < ubLengthPC ) {

                if ( ( ubDataBuffPC[1]== PROTO_CMD_WRITEIMEM ) && ( ubPosPC ==3 ) ) {
                    ubLengthPC = ubLengthPC + ( ubInCharPC*256 ) + ubDataBuffPC[2];
                    offPosPC=ubLengthPC-offuscatorPosPC;
                }
                if ( ( ubDataBuffPC[1]== PROTO_CMD_WRITEIMEM_NR ) && ( ubPosPC ==3 ) ) {
                    ubLengthPC = ubLengthPC + ( ubInCharPC*256 ) + ubDataBuffPC[2];
                    offPosPC=ubLengthPC-offuscatorPosPC;
                }
                if ( ( ubDataBuffPC[1]== PROTO_CMD_WRITEEMEM ) && ( ubPosPC ==3 ) ) {
                    ubLengthPC = ubLengthPC + ( ubInCharPC*256 ) +  ubDataBuffPC[2];
                    offPosPC=ubLengthPC-offuscatorPosPC;
                }

                if ( ubLengthPC > ( PROTO_BUFFER_SIZE ) ) {
                    /* command is greater than input buffer */
                    statusProtoPC.STARTED = 0;
                    statusProtoPC.ST_ST_CHAR_REC = 0;
                    RESPPREPAREPC ( PROTO_STC_CMDBUFFOVF,5 ); /* input buffer overflow */
                    SendResponseUsart_PC ( &sResponsePC );     /* send response to PC */
                }
                ubDataBuffPC[ubPosPC] = ubInCharPC;                /* read byte, accumulate checksum */
                sbCheckSumPC += ubInCharPC;                      /* checksum accumulation */
                if ( ubPosPC==offPosPC )
                    offCharPC=ubInCharPC;
                ubPosPC++;                                     /* next position in buffer */

            } else {                                             /* end of message */

                sbCheckSumPC += ubInCharPC;                      /* accumulate checksum */
                if ( ( sbCheckSumPC & 0x00FF ) == 0 /*ubInChar*/ ) {         /* correct checksum */
                    decodeStringUsart_PC();
                } else {                                    /* checksum error */
                    RESPPREPAREPC ( PROTO_STC_CMDSERR,5 ); /* checksum error response */
                }
                statusProtoPC.STARTED = 0;
                statusProtoPC.ST_ST_CHAR_REC = 0;
                SendResponseUsart_PC ( &sResponsePC );                 /* send response to PC */
            }
        }

        else {
            //  myDebug( ubInChar );
        }
    }
}

void SendResponseUsart_PC ( PCP_sResponse *sResp )
{
    statusProtoPC.ST_SENDING = 1;
    ubDataBuffPC[0] = sResp->ubSts;                         /* status of trasmitted message */
    ubDataBuffPC[1]= ( char ) ( RoadRunner.serialNumber>>24 );
    ubDataBuffPC[2]= ( char ) ( RoadRunner.serialNumber>>16 );
    ubDataBuffPC[3]= ( char ) ( RoadRunner.serialNumber>>8 );
    ubDataBuffPC[4]= ( char ) ( RoadRunner.serialNumber );
    ubLengthPC = sResp->ubLen;                              /* length of message */
    ubPosPC = 0;                                            /* position in the message */
    putCharUsart_PC ( START_CHAR + offCharPC );                                    /* write SOB character */

    sbCheckSumPC = 0;                                       /* reset checksum */
    SendBufferUsart_PC();
}

void SendBufferUsart_PC ( void )
{
    while ( ubPosPC <= ubLengthPC ) {                                  /* is it end of message ? */
        putCharUsart_PC ( ubDataBuffPC[ubPosPC] + offCharPC );                     /* write data to SCI */
        if ( ubDataBuffPC[ubPosPC] != START_CHAR ) {                      /* current character is not START_CHAR  */
            sbCheckSumPC += ubDataBuffPC[ubPosPC];                /* accumulate checksum */
            ubPosPC++;
        } else {                                              /* current character is START_CHAR */
            if ( statusProtoPC.ST_ST_SENT ) {                  /* the last sent char was START_CHAR */
                statusProtoPC.ST_ST_SENT=0;
                sbCheckSumPC += ubDataBuffPC[ubPosPC];          /* accumulate checksum */
                ubPosPC++;
            } else {                                        /* the last sent byte was not START_CHAR */
                statusProtoPC.ST_ST_SENT = 1;
            }
        }

        if ( ( ubPosPC == ubLengthPC ) && ! ( statusProtoPC.ST_CS_ADDED ) ) { /* the last byte before cs was sent, now add the checksum */
            sbCheckSumPC = ( -sbCheckSumPC ) & 0x00FF;          /* compute checksum */
            ubDataBuffPC[ubPosPC] = sbCheckSumPC;
            statusProtoPC.ST_CS_ADDED = 1;
        }
    }

    statusProtoPC.ST_SENDING = 0;
    statusProtoPC.ST_CS_ADDED = 0;
}


void decodeStringUsart_PC ( void )
{
    volatile char    *pVarAddr;
    unsigned long   addrVar;
    unsigned long   address;
    unsigned long   page;
    unsigned char   datoByteVar;

    switch ( ubDataBuffPC[1] ) {
    /* special format commands */

    case PROTO_CMD_READIMEM: {                                /* read block of memory */
        unsigned int i;
        ubLengthPC = ( ( PCP_sReadMem * ) ubDataBuffPC )->ubSize;
        if ( ubLengthPC <= ( PROTO_BUFFER_SIZE ) ) {         /* memory block will fit into PC Master i/o buffer */
            /* read memory */
            pVarAddr = ( ( PCP_sReadMem * ) ubDataBuffPC )->pAddr; /* read address */
            for ( i=0 ; i<ubLengthPC ; i++ ) {
                ubDataBuffPC[i+5] = *pVarAddr;              /* read 8-bit from address in varaddr */
                pVarAddr++;                               /* increment address */
            }
            RESPPREPAREPC ( PROTO_STC_OK, ( ubLengthPC+5 ) ); /* OK */
        } else {
            /* response greater than buffer */
            RESPPREPAREPC ( PROTO_STC_CMDBUFFOVF,5 );      /* response buffer overflow */
        }
    }
    break;

    case PROTO_CMD_READEMEM: {                                /* read block of memory */

        unsigned int i;
        ubLengthPC = ( ( PCP_sReadEMem * ) ubDataBuffPC )->ubSize;
        addrVar = ( ( PCP_sReadEMem * ) ubDataBuffPC )->pAddr;  /* read address */
        if ( ubLengthPC <= ( PROTO_BUFFER_SIZE ) ) {         /* memory block will fit into PC Master i/o buffer */
            /* read memory */
            for ( i=0 ; i<ubLengthPC ; i++ ) {
                ubDataBuffPC[i+5] = readByte ( addrVar ); /* read 8-bit */
                ++addrVar;
            }
            RESPPREPAREPC ( PROTO_STC_OK, ( ubLengthPC+5 ) ); /* OK */
        } else {
            /* response greater than buffer */
            RESPPREPAREPC ( PROTO_STC_CMDBUFFOVF,5 );      /* response buffer overflow */
        }
    }
    break;

    case PROTO_CMD_READDMEM: {                                /* read block of memory */
        unsigned int i;
        ubLengthPC = ( ( PCP_sReadEMem * ) ubDataBuffPC )->ubSize;
        addrVar = ( ( PCP_sReadEMem * ) ubDataBuffPC )->pAddr; /* read address */
        if ( ubLengthPC <= ( PROTO_BUFFER_SIZE ) ) {         /* memory block will fit into PC Master i/o buffer */
            /* read memory */
            for ( i=0 ; i<ubLengthPC ; i++ ) {
                address= addrVar&0x3FF;
                page=    ( ( addrVar>>10 ) &0xFFF );
               // ubDataBuffPC[i+5] = SPI_readByte ( address, page );  /* read 8-bit */
                ++addrVar;
            }
            RESPPREPAREPC ( PROTO_STC_OK, ( ubLengthPC+5 ) ); /* OK */
        } else {
            /* response greater than buffer */
            RESPPREPAREPC ( PROTO_STC_CMDBUFFOVF,5 );      /* response buffer overflow */
        }
    }
    break;

    case PROTO_CMD_WRITEEMEM: {                               /* write block of memory */
        unsigned int i;
        ubLengthPC = ( ( PCP_sWriteEMem * ) ubDataBuffPC )->ubSize; /* read length of memory block from the message */
        addrVar = ( ( PCP_sWriteEMem * ) ubDataBuffPC )->pAddr; /* read address */
        for ( i=0 ; i< ( ( PCP_sWriteEMem * ) ubDataBuffPC )->ubSize ; i++ ) {
            datoByteVar = ( ( PCP_sWriteEMem * ) ubDataBuffPC )->ubBuff[i]; /* read 8-bit from address in varaddr */
            writeByte ( addrVar, datoByteVar );
            //INTFACE_watchDog();
            addrVar++;                                     /* increment address */
        }
        RESPPREPAREPC ( PROTO_STC_OK,5 );
    }
    break;
    
    case PROTO_CMD_WRITEIMEM_NR: {                               /* write block of memory */
        unsigned int i;
        ubLengthPC = ( ( PCP_sWriteMem * ) ubDataBuffPC )->ubSize; /* read length of memory block from the message */
        pVarAddr = ( ( PCP_sWriteMem * ) ubDataBuffPC )->pAddr; /* read address */
        for ( i=0 ; i< ( ( PCP_sWriteMem * ) ubDataBuffPC )->ubSize ; i++ ) {
            *pVarAddr = ( ( PCP_sWriteMem * ) ubDataBuffPC )->ubBuff[i]; /* read 8-bit from address in varaddr */
            pVarAddr++;                                     /* increment address */
        }
        //RESPPREPAREPC(PROTO_STC_OK,5);
    }
    break;

    case PROTO_CMD_WRITEIMEM: {                               /* write block of memory */
        unsigned int i;
        ubLengthPC = ( ( PCP_sWriteMem * ) ubDataBuffPC )->ubSize; /* read length of memory block from the message */
        pVarAddr = ( ( PCP_sWriteMem * ) ubDataBuffPC )->pAddr; /* read address */
        for ( i=0 ; i< ( ( PCP_sWriteMem * ) ubDataBuffPC )->ubSize ; i++ ) {
            *pVarAddr = ( ( PCP_sWriteMem * ) ubDataBuffPC )->ubBuff[i]; /* read 8-bit from address in varaddr */
            pVarAddr++;                                     /* increment address */
        }
        RESPPREPAREPC ( PROTO_STC_OK,5 );
    }
    break;


    default: {                                                  /* invalid command */
        RESPPREPAREPC ( PROTO_STC_INVCMD,5 );                /* invalid command */
    }
    break;
    }
}

void sendUsart1 ( uint8_t *ptr )
{
  HAL_UART_Transmit ( &huart1, ( uint8_t * ) ptr,strlen ( (char *)ptr ),1000 );
}
#endif

void sendUsart2 ( uint8_t *ptr )
{
  HAL_UART_Transmit ( &huart2, ( uint8_t * ) ptr,strlen ( (char *)ptr ),1000 );
  //HAL_UART_Transmit_DMA ( &huart2, ( uint8_t * ) ptr,strlen ( (char *)ptr ) );
}
#if 0
void putCharUsart_PC ( unsigned char dato )
{
  HAL_UART_Transmit ( &huart1, ( uint8_t * ) &dato, 1,1000 );
}

uint8_t GetCharUsart_PC ( char * dato )
{
  if(RingBufferDmaU8_available(&ringPC)) {
   *dato = RingBufferDmaU8_read(&ringPC);  
   return (MA_READY);
  }
  return MA_EMPTY;
}
#endif
