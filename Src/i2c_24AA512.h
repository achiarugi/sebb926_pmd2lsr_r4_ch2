#ifndef __I2C_SW_H
#define __I2C_SW_H

#include "stm32l4xx_hal.h"
#include "sensit.h"

#define     EEP_AA512_ADDR_U8           (0x54 << 1) 


void writeFloat ( uint32_t addr, float dato );
void writeLong ( uint32_t addr, uint32_t dato );
uint16_t readWord ( uint32_t addr );
float  readFloat ( uint32_t addr );
uint32_t readLong ( uint32_t addr );
void writeByte ( uint32_t addr ,uint8_t dato );
void writeWord ( uint32_t addr, uint16_t dato );
uint8_t readByte ( uint32_t addr );
void incLong ( uint32_t addr );
void incFloat ( uint32_t addr );

void readBuffer (  uint8_t *  buf, uint32_t addr,uint16_t nr );
void writeBuffer (  uint8_t *  buf, uint32_t addr,uint16_t nr );
double  readDouble ( uint32_t addr );
void writeDouble ( uint32_t addr, double dato );



#endif /* __I2C_SW_H */

