#ifndef __I2C_DIGITALPOT_AD5243_H
#define __I2C_DIGITALPOT_AD5243_H

#include "stm32l4xx_hal.h"
#define TRIAL_COUNT     (3)
#define COMM_TIMEOUT    (1000)

typedef enum {
	AD5243_WIPER_A            = 0x00,
	AD5243_WIPER_B            = 0x80,
} AD5243_SelectWiper;

// 7-bit address left shift is required
#define AD5243_ADDR     (0x5E )

HAL_StatusTypeDef AD5243_is_ready(I2C_HandleTypeDef *phi2c, uint16_t DevAddress);
HAL_StatusTypeDef AD5243_set_potentiometer_value(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, AD5243_SelectWiper wiper, uint16_t value, uint16_t shutDown);
HAL_StatusTypeDef ad5693_get_potentiometer(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, AD5243_SelectWiper wiper, uint8_t *buf_read_back, uint16_t shutDown);
#endif
