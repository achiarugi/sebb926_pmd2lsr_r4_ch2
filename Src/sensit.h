#ifndef SENSIT_H
#define SENSIT_H

#include "stdint.h"

#define     FALSE       0
#define     TRUE        1

#define     OFF      0
#define     ON       1

#define     BUTTON_ON      0
#define     BUTTON_OFF     1

#define     LOW      0
#define     HIGH		1

typedef union {
    uint8_t buf[2];
    uint16_t  uiVar;
} unionTwo;

typedef union {
    uint8_t buf[4];
    uint16_t  uiVar;
    uint32_t ulVar;
    int32_t          lVar;
    float         fVar;
    struct {
        uint16_t  wordVar_1;
        uint16_t  wordVar_2;
    } word;
} unionFour;

extern unionFour        unsized_1;
extern unionFour        unsized_2;
typedef union {
    uint8_t buf[8];
    double        dVar;
} unionEight;


//---------------------------------------------------------------------
// Struttura per la definizione della versione del firmware
typedef struct {
    uint16_t  release;        // 2 byte /**< Release */
    uint16_t  versione;       // 2 byte /**< Version */
} typeVersion;

//---------------------------------------------------------------------
// Struttura per la definizione della versione della data di calibrazione
typedef struct {
    uint8_t sysDay;         // 1 byte /**< Day of the month - [1,31] */
    uint8_t sysMonth;       // 1 byte /**< Months since January - [0,11] */
    uint16_t sysYear;         // 2 byte /**< Years */
} typeCalibrationDate;

//---------------------------------------------------------------------
// Struttura per la definizione dell'orologio interno
typedef struct {
    uint8_t sysDay;         // 1 byte /**< Day of the month - [1,31] */
    uint8_t sysMonth;       // 1 byte /**< Months since January - [0,11] */
    uint8_t sysYear;        // 1 byte /**< Years since 1900 */
    uint8_t sysHour;        // 1 byte /**< Hours since midnight - [0,23] */
    uint8_t sysMin;         // 1 byte /**< Minutes after the hour - [0,59] */
    uint8_t sysSec;         // 1 byte /**< Seconds after the minute - [0,59] */
} typeOrologio;

typedef struct {
    uint8_t state;		// 72 - 1 byte con un flag gestisco tutti i possibili stati , 255 possibili segnalazioni differenti} typeInternalStructure;
    uint32_t serialNumber;		// 73 - 4 
    typeVersion  	version;		// 77 - 4 byte
    char tmpBuf[ 256 ];			// 85 - 256  /buffer per debug strumento	
    char errorFlag;
} RoadRunner_Struct;



typedef struct{
  uint16_t			ID;								//0 - 2 laser ID
	float         temperatureW;			//2 - 4 working laser temperature
  float	        currentHSW;				//6 - 4 working laser current
  float	        rampAmplitudeHSW;	//10 - 4 amplitude of the scan
  uint16_t			triggerLevelHSW;	//14 - 2 trigger level to start acquisition
  float 				rampFrequencyW;		//16 - 4 ramp frequency
  float	        currentLSW;				//20 - 4 working laser current after jump
  float	        rampAmplitudeLSW;	//24 - 4 amplitude of the scan after jump
  uint16_t			triggerLevelLSW;	//28 - 2 trigger level to start acquisition after jump
  float	        temperatureR;			//30 - 4 realtime temperature on the NTC 
  float	        currentLaserR;		//34 - 4 realtime current on the laser diode
  float	        voltageR;					//38 - 4 realtime voltage on the laser diode
  float	        currentTecR;			//42 - 4 realtime current on the peltier cell
  float	        temperatureCellR;	//46 - 4 realtime temperature on the Herriot Cell
  uint8_t				tecStatus;				//50 - 1 temperature stabilization on/off (1/0)
  uint8_t				laserStatus;			//51 - 1 laser on/off(1/0)
}laser_struct;


typedef struct{
  uint16_t  		nAvgW;
  uint8_t  			centerFlag;
  uint16_t 			nAcquisitionPointsW;
  uint16_t 			dCLevelW;
	uint16_t				amplResistorValue;
  float 				convertionFactorW;
  uint8_t  			ready;
	uint8_t  			rampStatus;
	uint8_t 			switchRange;
	uint8_t 			autoBackground;
	uint8_t 			autoRange;
	uint16_t			zerophd;
}measurement_struct;


typedef struct{
  uint8_t  		nV;
  uint8_t  		printFLag;
  uint16_t  	nStab;
  uint32_t 		adcValueOriginal[512];
  uint32_t 		adcValueElaborated[512];
  uint32_t 		adcValueFinal[512];
	uint8_t  		triggerRampa;
	uint16_t 		waveform[1024];
}service_struct;


typedef struct{
  uint32_t 	timeStamp;					//0 - 4 timeStamp
  float			concValue;					//4 - 4 absolute concentration 
  float    	concValueRel; 			//8 - 4 relative concentration
	float			concMean;
  uint8_t		relAbsFlag;					//12 - 1 relative or absolute visualisation
  uint16_t 	laserState;					//13 - 2 state of the laser board
  uint8_t		lineState;					//15 - 1 in which line is emitting the laser
  uint8_t		sensitivityLevel;   //16 - 1 high or low sensitivity
  float     limite;							//17 - 4 ??????
  uint8_t		release;						//21 - 1 release 
  uint8_t		version;						//22 - 1 version
  uint8_t   errorType;          //23 - 1 version
  float     zero;								//24 - 4 version
	float 		power;
	float 		bit;
	uint16_t	nprint;
}realTime_struct;


typedef struct{
  uint16_t 	middle_point;
  uint16_t 	start_point;
  uint16_t 	end_point;
  uint16_t 	semi_area_point;
  uint8_t  	tipo;
  uint8_t  	nsm;
	uint8_t		sep;
	float 		calibHigh;
	float 		calibLow;
} analysis_struct;

extern laser_struct laser;
extern measurement_struct measurement;
extern realTime_struct realTime;
extern service_struct service;
extern RoadRunner_Struct RoadRunner;
extern analysis_struct analysis;
extern volatile int global_triggerRampa;
extern const uint16_t wave_point_table[1024];

#endif

