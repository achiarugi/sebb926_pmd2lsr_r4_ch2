#ifndef __DATEEPOCH_H
#define __DATEEPOCH_H
#include "time.h"

extern RTC_DateTypeDef epoch_sdate;
extern RTC_TimeTypeDef epoch_stime;
extern  uint32_t epochTimeStamp;

uint32_t RTC_ToEpoch(RTC_TimeTypeDef *time, RTC_DateTypeDef *date);
void RTC_FromEpoch(uint32_t epoch, RTC_TimeTypeDef *time, RTC_DateTypeDef *date);
void RTC_AdjustTimeZone(RTC_TimeTypeDef *time, RTC_DateTypeDef *date, int8_t offset);
void RTC_CalcDOW(RTC_DateTypeDef *date);
uint32_t epochTimeStamp_now(RTC_HandleTypeDef *hrtcEpoch);

void getCurrentDate(struct tm *date);
time_t getCurrentUnixTime(void);
void convertUnixTimeToDate(time_t t, struct tm *date);
time_t convertDateToUnixTime(const struct tm *date);

uint8_t computeDayOfWeek(uint16_t y, uint8_t m, uint8_t d);

#endif
