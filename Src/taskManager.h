#ifndef TASKMANAGER_H
#define TASKMANAGER_H

extern void ( *ptrTaskManager ) ( void );

void    powerUp_state( void );
void 	initStructure_state( void );
void    writeParameters_state( void );
void    startTemperatureControl_state(void);
void    waitControlTemperatureOne_state(void);
void    controlTemperatureOne_state(void);
void    controlTemperatureTwo_state(void);
void    startRamp_state(void);
void    dummy_state(void);
void    test_state(void);
void    print_n_values(uint32_t *pvalues, uint16_t n_values);
void    print_n_values_2(uint32_t *pvalues, uint16_t n_values);
void		print_n_values_3(uint32_t *pvalues, uint16_t n_values);
void 		print_conc(void);
void    choose_DC_level(void);
void    warming_housekeeping_state(void);
void    measurement_housekeeping_state(void);
void    measurement_state(void);
void    communication_state(void);
void    set_parameters_state(void);
void    calculate_concentration(void);
void 		calculate_concentration_diff(void);
void    change_settings_state(void);
void    linearFit(void);
void    linearFit2(void);
void    frange(void);
void    line_center(void);
void    error_state(void);
void    taskSPI_communication(void);
void 		load_struct(void);
void 		set_sensitivity_level(void);
void 		write_laser_parameters(void);
void    smooth(uint8_t nsm, uint32_t *x, uint16_t dim);
void 		meanv(uint32_t *v,	float *y_mean, uint16_t dim);
void 		minV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin);
void 		minmaxV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax);
void 		minmaxmeanV(uint32_t *v,uint16_t dim,float *y_mean, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax);

void 		init_BLE_UART(void);
void 		print_BLE_UART(void);

#define  Tc_dummy_state                      1L 
#define  Tc_test_state                       50L 
#define  Tc_powerUp_state                    10000L
#define  Tc_initStructure_state              1000L  
#define  Tc_housekeeping_state               3000L  
#define  Tc_measurement_state                1000L
#define  Tc_communication_state              1000L  
#define  Tc_writeParameters_state            1000L  
#define  Tc_startTemperatureControl_state    1000L  
#define  Tc_waitControlTemperatureOne_state  1000L  
#define  Tc_controlTemperatureOne_state      1000L  
#define  Tc_controlTemperatureTwo_state      1000L 
#define  Tc_startRamp_state                  1000L 
#define  Tc_choose_DC_level                  100L 
#define  Tc_measurement                      100L 
#define  Tc_warming_state                    1000L
#define  Tc_dummy_state                    1000L

#endif
