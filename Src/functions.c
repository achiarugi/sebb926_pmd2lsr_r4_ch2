#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "functions.h"
#include "ad.h"
#include "eprom_address.h"
#include "lcd_St7529.h"
#include "i2c_24AA512.h"
#include "lcd.h"
#include "text.h"
#include "sensit.h"
#include "main.h"
#include "menu.h"
#include "out.h"


unsigned int    tim1msExecuteMenu = 0;

    unsigned int iTmp,i,j,molt;
	int pointX, pointY, pointXnew, pointYnew;
	float flt;
    int conta;
	unsigned char tmpBufold[528]; 
	unsigned long lng;

    unsigned char rxChar;
	
	int max1;
	int min1;
	int max2;
	int min2;
	
	int delta;
	
	int idx;
	int array[20];

//Inizializzo tutta la struttura RAM dello strumento, caricando anche la calibrazione
void Init_FPOM_struct(void)
{
    /*inizializzo la tastiera*/
  	BUTTON_1=0;
	BUTTON_2=0;
	BUTTON_3=0;
	BUTTON_4=0;
        
    /*inizializzo la struttura Sensit*/
    S.unionFPOM.FPOMStruct.serialNumber 	= 	readLong( EEP_SERIAL_NUMBER); 
  
    /*inizializzo la struttura instrument*/
        S.instrument.id                     =      S.unionFPOM.FPOMStruct.serialNumber;
	S.instrument.contrast           =       0;
	S.instrument.pumpstatus              =       0;
	S.instrument.pumpSetPower                =       0;
	S.instrument.gasSetPressure          =       readFloat(EEP_RAPP_GAIN );
	S.instrument.bottleSetPressure	        = 	readByte( EEP_MEAS_GAIN); 
	S.instrument.videogain	=	readByte( EEP_NUM_CICLI); 
	S.instrument.tipo_analisi	= 	readByte( EEP_INTERVALLO); 
	S.instrument.tipo_auto             = 	readWord( EEP_MEAS_POINTER); 
	S.instrument.intervalMeasure	                = 	0;
	S.instrument.intervalCalibration 	        = 	readWord( EEP_CAL_POINTER); 
        //S.instrument.intervalTest            = 	readWord( EEP_CAL_POINTER); 
        S.instrument.gain          = 	readWord( EEP_CAL_POINTER); 
        
        S.instrument.slope                     =      S.unionFPOM.FPOMStruct.serialNumber;
	S.instrument.resolution           =       0;
	S.instrument.intEnable              =       0;
	S.instrument.intDisable                =       0;
	S.instrument.duration          =       readWord(EEP_RAPP_GAIN );
	S.instrument.autoZeroStart	        = 	readByte( EEP_MEAS_GAIN); 
	S.instrument.durationAutozero	=	readByte( EEP_NUM_CICLI); 
	S.instrument.meas_gain	= 	readByte( EEP_INTERVALLO); 
	S.instrument.meas_point             = 	readWord( EEP_MEAS_POINTER); 
	S.instrument.nrMemoryData	                = 	0;
	S.instrument.cal_gain 	        = 	readWord( EEP_CAL_POINTER); 
        S.instrument.cal_point            = 	readWord( EEP_CAL_POINTER); 
        S.instrument.cal_number          = 	readWord( EEP_CAL_POINTER); 
        
        S.instrument.rapporto_Gain                     =      S.unionFPOM.FPOMStruct.serialNumber;
	//S.instrument.Cod_operatore[40]           =       ;
	S.instrument.cod_latitude              =       0;
	S.instrument.cod_longitude                =       0;
	//S.instrument.Cod_impianto[40]          =       readFloat(EEP_RAPP_GAIN );
	//S.instrument.Cod_p_prelievo[40]	        = 	readByte( EEP_MEAS_GAIN); 
	//S.instrument.Cod_odorizzante[40]	=	readByte( EEP_NUM_CICLI); 
	S.instrument.temp_Col	= 	readByte( EEP_INTERVALLO); 
	S.instrument.tcolnow             = 	readWord( EEP_MEAS_POINTER); 
	S.instrument.temp1	                = 	0;
	S.instrument.temp2 	        = 	readWord( EEP_CAL_POINTER); 
        if (S.instrument.temp_Col>100) S.instrument.temp_Col=99;
      
        /*inizializzo la struttura Calibration*/
	LoadCalibration();
}

//carico calibrazione da eeprom
void LoadCalibration(){
 

  S.calibration.ID			=	readLong(  EEP_CAL_ID); 
  S.calibration.Temp_Col		=	readLong(  EEP_CAL_ID); 
  S.calibration.Concentrazione		=	readLong(  EEP_CAL_ID); 
  S.calibration.Area      		=	readLong(  EEP_CAL_ID); 
  S.calibration.Gasflow		        =       readFloat( EEP_CAL_AZ_VALUE);
  S.calibration.Pumpflow		=	readFloat( EEP_CAL_GASFLOW); 
  S.calibration.Videogain		=	readWord( EEP_CAL_PUMPFLOW);
  S.calibration.Gain	                =	readWord( EEP_CAL_PUMPFLOW); 
  S.calibration.slope		        =	readByte( EEP_CAL_VIDEOGAIN ); 
  S.calibration.Resolution		=	readByte( EEP_CAL_GAIN ); 
  S.calibration.AutoZeroValue		=       readWord( EEP_CAL_SLOPE ); 
  S.calibration.IntEnable		=	readWord( EEP_CAL_INTENABLE); 
  S.calibration.IntDisable		=	readWord( EEP_CAL_INTDISABLE); 
  S.calibration.Duration		=	readWord( EEP_CAL_DURATA ); 
  S.calibration.AutozeroStart		=	readWord( EEP_CAL_AUTOZERO ); 
  S.calibration.DurationAutozero	=	readWord( EEP_CAL_DURATA_AZ ); 
  S.calibration.Hour		        =	readByte( EEP_CAL_GAIN ); 
  S.calibration.Minutes		        =       readWord( EEP_CAL_SLOPE ); 
  S.calibration.Day		        =	readWord( EEP_CAL_INTENABLE); 
  S.calibration.Month		        =	readWord( EEP_CAL_INTDISABLE); 
  S.calibration.Year		        =	readWord( EEP_CAL_DURATA ); 
  S.calibration.PeakArea		=	readWord( EEP_CAL_AUTOZERO ); 
  S.calibration.PeakStart	        =	readWord( EEP_CAL_DURATA_AZ ); 
  S.calibration.PeakMax		        =	readWord( EEP_CAL_AUTOZERO ); 
  S.calibration.PeakStop	        =	readWord( EEP_CAL_DURATA_AZ ); 


}

//salvo calibrazione in eeprom
void SalvaCalibrazioneEEPROM(char cal_num){


    
sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf,( const void * )txt_Salv);
LCD_centerMessageBuf(Arial_8_BOLD, (char *)S.unionFPOM.FPOMStruct.tmpBuf);

 
/*
writeBuffersw( EEP_CAL_COD_OPERATORE,(uint8_t *)S.Calibration.cod_operatore,40);
writeBuffersw( EEP_CAL_COD_LOCALITA,(uint8_t *)S.Calibration.cod_localita,40);
writeBuffersw( EEP_CAL_COD_IMPIANTO,(uint8_t *)S.Calibration.cod_impianto,40);
writeBuffersw( EEP_CAL_COD_P_PRELIEVO,(uint8_t *)S.Calibration.cod_p_prelievo,40);
writeBuffersw( EEP_CAL_COD_ODORIZZANTE,(uint8_t *)S.Calibration.cod_odorizzante,40);
writeFloat( EEP_CAL_TEMP,S.Calibration.Temp_Col );
writeFloat( EEP_CAL_GASFLOW,S.Calibration.Gasflow); 
writeFloat( EEP_CAL_PUMPFLOW,S.Calibration.Pumpflow);
writeByte( EEP_CAL_UNITS,S.Calibration.Units );  
writeByte( EEP_CAL_VIDEOGAIN,S.Calibration.Videogain ); 
writeByte( EEP_CAL_GAIN,S.Calibration.Gain ); 
writeByte( EEP_CAL_PICCHI,S.Calibration.Picchi ); 
writeByte( EEP_CAL_TIPO_COL,S.Calibration.Tipo_col );
writeByte( EEP_CAL_ORA,S.unionFPOM.FPOMStruct.orologio.sysHour );
writeByte( EEP_CAL_MINUTI,S.unionFPOM.FPOMStruct.orologio.sysMin );
writeByte( EEP_CAL_GIORNO,S.unionFPOM.FPOMStruct.orologio.sysDay );
writeByte( EEP_CAL_MESE,S.unionFPOM.FPOMStruct.orologio.sysMonth );
writeByte( EEP_CAL_ANNO,S.unionFPOM.FPOMStruct.orologio.sysYear ); 
writeWord( EEP_CAL_SLOPE,S.Calibration.Slope ); 
writeWord( EEP_CAL_RES,S.Calibration.Res ); 	
writeByte( EEP_CAL_NUM,S.Calibration.Cal_number);
writeWord( EEP_CAL_DURATA),S.Calibration.Duration ); 
writeLong( EEP_CAL_ID,S.Calibration.ID	); 
writeLong( EEP_CAL_AREA,S.Calibration.Area); 
writeFloat( EEP_CAL_CONC,S.Calibration.Concentrazione);
writeWord( EEP_CAL_INTENABLE,S.Calibration.IntEnable ); 
writeWord( EEP_CAL_INTDISABLE,S.Calibration.IntDisable ); 
writeWord( EEP_CAL_AUTOZERO,S.Calibration.Autozero ); 
writeWord( EEP_CAL_DURATA_AZ,S.Calibration.durationAutozero ); 
writeFloat(EEP_CAL_AZ_VALUE,S.Calibration.AutoZeroValue );
writeLong(EEP_CAL_AREA1,	  S.Calibration.PeakArea ); 
writeWord( EEP_CAL_INIZIO1,	  S.Calibration.PeakStart); 
writeWord( EEP_CAL_MASSIMO1,  S.Calibration.PeakMax ); 
writeWord( EEP_CAL_FINE1,	  S.Calibration.PeakStop ); 
*/
}

//salvo misura in dataflash
void SalvaMisura(){
  /*
//esegue il salvataggio della misura in DATA FLASH
	unsigned long i, page;

	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf,txt_Salv);
	LCD_centerMessageBuf(Arial_8_BOLD, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	S.instrument.meas_point++;
	//adesso viene fatto il salvataggio
	SPI_ClearBuf();
 	resetWatchDog();
	page= ((SPI_MEAS_OFFSET * (S.instrument.meas_point-1))+SPI_MEAS_PAGE_INIT);
	//page=00;
	SPI_writeFloat( SPI_MEAS_TEMP,S.Measure.Temp_Col);
	SPI_writeLong( SPI_MEAS_ID,S.Measure.ID); 
	SPI_writeLong( SPI_MEAS_AREA,S.Measure.Area); 
	SPI_writeFloat(SPI_MEAS_CONC,S.Measure.Concentrazione); 
	SPI_writeFloat( SPI_MEAS_GASFLOW,S.Measure.Gasflow); 
	SPI_writeFloat( SPI_MEAS_PUMPFLOW,S.Measure.Pumpflow);
	SPI_writeByte( SPI_MEAS_UNITS,S.Measure.Units); 
	SPI_writeByte( SPI_MEAS_VIDEOGAIN,S.Measure.Videogain); 
	SPI_writeByte( SPI_MEAS_GAIN,S.Measure.Gain); 
	SPI_writeByte( SPI_MEAS_PICCHI,S.Measure.Picchi); 
	SPI_writeByte( SPI_MEAS_TIPO_COL,S.instrument.Tipo_col);
	SPI_writeByte( SPI_MEAS_ORA,S.Measure.Ora);
	SPI_writeByte( SPI_MEAS_MINUTI,S.Measure.Minuti);
	SPI_writeByte( SPI_MEAS_GIORNO,S.Measure.Giorno);
	SPI_writeByte( SPI_MEAS_MESE,S.Measure.Mese);
	SPI_writeByte( SPI_MEAS_ANNO,S.Measure.Anno); 
	SPI_writeWord( SPI_MEAS_SLOPE,S.Measure.Slope); 
	SPI_writeWord( SPI_MEAS_RES,S.Measure.Res); 
	SPI_writeWord( SPI_MEAS_INTENABLE,S.Measure.IntEnable); 
	SPI_writeWord( SPI_MEAS_INTDISABLE,S.Measure.IntDisable); 
	SPI_writeWord( SPI_MEAS_DURATA,S.Measure.Duration); 
	SPI_writeWord( SPI_MEAS_AUTOZERO,S.Measure.Autozero); 
	SPI_writeWord( SPI_MEAS_DURATA_AZ,S.Measure.durationAutozero); 
	SPI_writeFloat( SPI_MEAS_AZ_VALUE,S.Measure.AutoZeroValue);

	SPI_writeLong( SPI_MEAS_CAL_AREA,S.Measure.Cal_Area); 
	SPI_writeFloat(SPI_MEAS_CAL_CONC,S.Measure.Cal_Concentrazione); 
	SPI_writeByte( SPI_MEAS_CAL_ORA,(readByte( OFFSET + EEP_CAL_ORA)));
	SPI_writeByte( SPI_MEAS_CAL_MINUTI,	(readByte( OFFSET+EEP_CAL_MINUTI )));
	SPI_writeByte( SPI_MEAS_CAL_GIORNO,(readByte( OFFSET+EEP_CAL_GIORNO )));
	SPI_writeByte( SPI_MEAS_CAL_MESE,(readByte( OFFSET+EEP_CAL_MESE )));
	SPI_writeByte( SPI_MEAS_CAL_ANNO,(readByte( OFFSET+EEP_CAL_ANNO ))); 
	SPI_writeByte( SPI_MEAS_CAL_GAIN,(readByte( OFFSET+EEP_CAL_GAIN )));

 	resetWatchDog();
	SPI_WriteDataBuf( SPI_MEAS_PICCO1,(uint8_t *)S.Measure.Picco[0].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA1,	S.Measure.Picco[0].area); 
	SPI_writeWord( SPI_MEAS_INIZIO1,	S.Measure.Picco[0].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO1,	S.Measure.Picco[0].massimo); 
	SPI_writeWord( SPI_MEAS_FINE1,	S.Measure.Picco[0].fine); 

	SPI_WriteDataBuf( SPI_MEAS_PICCO2,(uint8_t *)S.Measure.Picco[1].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA2,	S.Measure.Picco[1].area); 
	SPI_writeWord( SPI_MEAS_INIZIO2,	S.Measure.Picco[1].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO2,	S.Measure.Picco[1].massimo); 
	SPI_writeWord( SPI_MEAS_FINE2,	S.Measure.Picco[1].fine); 

	SPI_WriteDataBuf( SPI_MEAS_PICCO3,(uint8_t *)S.Measure.Picco[2].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA3,	S.Measure.Picco[2].area); 
	SPI_writeWord( SPI_MEAS_INIZIO3,	S.Measure.Picco[2].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO3,	S.Measure.Picco[2].massimo); 
	SPI_writeWord( SPI_MEAS_FINE3,	S.Measure.Picco[2].fine); 

	SPI_WriteDataBuf( SPI_MEAS_PICCO4,(uint8_t *)S.Measure.Picco[3].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA4,	S.Measure.Picco[3].area); 
	SPI_writeWord( SPI_MEAS_INIZIO4,	S.Measure.Picco[3].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO4,	S.Measure.Picco[3].massimo); 
	SPI_writeWord( SPI_MEAS_FINE4,	S.Measure.Picco[3].fine); 
 	
	resetWatchDog();
	SPI_WriteDataBuf( SPI_MEAS_PICCO5,(uint8_t *)S.Measure.Picco[4].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA5,	S.Measure.Picco[4].area ); 
	SPI_writeWord( SPI_MEAS_INIZIO5,	S.Measure.Picco[4].inizio ); 
	SPI_writeWord( SPI_MEAS_MASSIMO5,	S.Measure.Picco[4].massimo ); 
	SPI_writeWord( SPI_MEAS_FINE5,	S.Measure.Picco[4].fine ); 

	SPI_WriteDataBuf( SPI_MEAS_COD_OPERATORE,(uint8_t *)S.Measure.cod_operatore,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_LOCALITA,(uint8_t *)S.Measure.cod_localita,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_IMPIANTO,(uint8_t *)S.Measure.cod_impianto,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_P_PRELIEVO,(uint8_t *)S.Measure.cod_p_prelievo,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_ODORIZZANTE,(uint8_t *)S.Measure.cod_odorizzante,40);
 	resetWatchDog();
	SPI_WritePage (page);

	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[i]);
		}
	page++;
	SPI_WritePage (page);

	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[(i+264)]);
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[(i+528)]);
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[(i+792)]);
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();
	
S.instrument.Meas=S.instrument.Meas_point;

//aggiorno in eeprom il numero di misure effettuate
writeWord( EEP_MEAS_POINTER,S.instrument.Meas_point ); 
*/
}

//salvo test in dataflash
void SalvaTest(){
 /*
//esegue il salvataggio della misura in DATA FLASH
	unsigned long i, page;
 
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf,txt_Salv);
	LCD_centerMessageBuf(Arial_8_BOLD, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	S.instrument.meas_point++;
	//adesso viene fatto il salvataggio
	SPI_ClearBuf();
 	resetWatchDog();
	page= ((SPI_MEAS_OFFSET * (S.instrument.meas_point-1))+SPI_MEAS_PAGE_INIT);
	//page=00;
	SPI_writeFloat( SPI_MEAS_TEMP,S.Measure.Temp_Col);
	SPI_writeLong( SPI_MEAS_ID,S.Measure.ID); 
	SPI_writeLong( SPI_MEAS_AREA,S.Measure.Area); 
	SPI_writeFloat(SPI_MEAS_CONC,S.Measure.Concentrazione); 
	SPI_writeFloat( SPI_MEAS_GASFLOW,S.Measure.Gasflow); 
	SPI_writeFloat( SPI_MEAS_PUMPFLOW,S.Measure.Pumpflow);
	SPI_writeByte( SPI_MEAS_UNITS,S.Measure.Units); 
	SPI_writeByte( SPI_MEAS_VIDEOGAIN,S.Measure.Videogain); 
	SPI_writeByte( SPI_MEAS_GAIN,S.Measure.Gain); 
	SPI_writeByte( SPI_MEAS_PICCHI,S.Measure.Picchi); 
	SPI_writeByte( SPI_MEAS_TIPO_COL,S.instrument.Tipo_col);
	SPI_writeByte( SPI_MEAS_ORA,S.Measure.Ora);
	SPI_writeByte( SPI_MEAS_MINUTI,S.Measure.Minuti);
	SPI_writeByte( SPI_MEAS_GIORNO,S.Measure.Giorno);
	SPI_writeByte( SPI_MEAS_MESE,S.Measure.Mese);
	SPI_writeByte( SPI_MEAS_ANNO,S.Measure.Anno); 
	SPI_writeWord( SPI_MEAS_SLOPE,S.Measure.Slope); 
	SPI_writeWord( SPI_MEAS_RES,S.Measure.Res); 
	SPI_writeWord( SPI_MEAS_INTENABLE,S.Measure.IntEnable); 
	SPI_writeWord( SPI_MEAS_INTDISABLE,S.Measure.IntDisable); 
	SPI_writeWord( SPI_MEAS_DURATA,S.Measure.Duration); 
	SPI_writeWord( SPI_MEAS_AUTOZERO,S.Measure.Autozero); 
	SPI_writeWord( SPI_MEAS_DURATA_AZ,S.Measure.durationAutozero); 
	SPI_writeFloat( SPI_MEAS_AZ_VALUE,S.Measure.AutoZeroValue);

	SPI_writeLong( SPI_MEAS_CAL_AREA,S.Measure.Cal_Area); 
	SPI_writeFloat(SPI_MEAS_CAL_CONC,S.Measure.Cal_Concentrazione); 
	SPI_writeByte( SPI_MEAS_CAL_ORA,(readByte( OFFSET + EEP_CAL_ORA)));
	SPI_writeByte( SPI_MEAS_CAL_MINUTI,	(readByte( OFFSET+EEP_CAL_MINUTI )));
	SPI_writeByte( SPI_MEAS_CAL_GIORNO,(readByte( OFFSET+EEP_CAL_GIORNO )));
	SPI_writeByte( SPI_MEAS_CAL_MESE,(readByte( OFFSET+EEP_CAL_MESE )));
	SPI_writeByte( SPI_MEAS_CAL_ANNO,(readByte( OFFSET+EEP_CAL_ANNO ))); 
	SPI_writeByte( SPI_MEAS_CAL_GAIN,(readByte( OFFSET+EEP_CAL_GAIN )));

 	resetWatchDog();
	SPI_WriteDataBuf( SPI_MEAS_PICCO1,(uint8_t *)S.Measure.Picco[0].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA1,	S.Measure.Picco[0].area); 
	SPI_writeWord( SPI_MEAS_INIZIO1,	S.Measure.Picco[0].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO1,	S.Measure.Picco[0].massimo); 
	SPI_writeWord( SPI_MEAS_FINE1,	S.Measure.Picco[0].fine); 

	SPI_WriteDataBuf( SPI_MEAS_PICCO2,(uint8_t *)S.Measure.Picco[1].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA2,	S.Measure.Picco[1].area); 
	SPI_writeWord( SPI_MEAS_INIZIO2,	S.Measure.Picco[1].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO2,	S.Measure.Picco[1].massimo); 
	SPI_writeWord( SPI_MEAS_FINE2,	S.Measure.Picco[1].fine); 

	SPI_WriteDataBuf( SPI_MEAS_PICCO3,(uint8_t *)S.Measure.Picco[2].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA3,	S.Measure.Picco[2].area); 
	SPI_writeWord( SPI_MEAS_INIZIO3,	S.Measure.Picco[2].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO3,	S.Measure.Picco[2].massimo); 
	SPI_writeWord( SPI_MEAS_FINE3,	S.Measure.Picco[2].fine); 

	SPI_WriteDataBuf( SPI_MEAS_PICCO4,(uint8_t *)S.Measure.Picco[3].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA4,	S.Measure.Picco[3].area); 
	SPI_writeWord( SPI_MEAS_INIZIO4,	S.Measure.Picco[3].inizio); 
	SPI_writeWord( SPI_MEAS_MASSIMO4,	S.Measure.Picco[3].massimo); 
	SPI_writeWord( SPI_MEAS_FINE4,	S.Measure.Picco[3].fine); 
 	
	resetWatchDog();
	SPI_WriteDataBuf( SPI_MEAS_PICCO5,(uint8_t *)S.Measure.Picco[4].nome_picco,5); 
	SPI_writeLong( SPI_MEAS_AREA5,	S.Measure.Picco[4].area ); 
	SPI_writeWord( SPI_MEAS_INIZIO5,	S.Measure.Picco[4].inizio ); 
	SPI_writeWord( SPI_MEAS_MASSIMO5,	S.Measure.Picco[4].massimo ); 
	SPI_writeWord( SPI_MEAS_FINE5,	S.Measure.Picco[4].fine ); 

	SPI_WriteDataBuf( SPI_MEAS_COD_OPERATORE,(uint8_t *)S.Measure.cod_operatore,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_LOCALITA,(uint8_t *)S.Measure.cod_localita,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_IMPIANTO,(uint8_t *)S.Measure.cod_impianto,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_P_PRELIEVO,(uint8_t *)S.Measure.cod_p_prelievo,40);
	SPI_WriteDataBuf( SPI_MEAS_COD_ODORIZZANTE,(uint8_t *)S.Measure.cod_odorizzante,40);
 	resetWatchDog();
	SPI_WritePage (page);

	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[i]);
		}
	page++;
	SPI_WritePage (page);

	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[(i+264)]);
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[(i+528)]);
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();

	for (i=0;i<264;i++){ 
		resetWatchDog();
		SPI_writeWord( (i*2),S.Measure.Data[(i+792)]);
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();
	
S.instrument.Meas=S.instrument.Meas_point;

//aggiorno in eeprom il numero di misure effettuate
writeWord( EEP_MEAS_POINTER,S.instrument.Meas_point ); 
*/
}

//salvo calibrazione in dataflash
void SalvaCalibration(unsigned char calibrazione){
 /*
	//esegue il salvataggio della calibrazione in DATA FLASH
	unsigned long i, page, j;
	unsigned char tmp[40];


	if (S.instrument.Cal_point<SPI_OLDCAL_MAX){
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf,txt_Back);
	LCD_centerMessageBuf(Arial_8_BOLD, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	 resetWatchDog();


	SPI_ClearBuf();

	page= (SPI_OLDCAL_OFFSET * S.instrument.Cal_point);
	OFFSET= ((unsigned long)EEP_CAL_OFFSET * (unsigned long)S.instrument.Tipo_col) + EEP_CAL_INIT;

	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, "PRIMA PAGINA : %lu %lu %lu %lu\r\n",page,(unsigned long) EEP_CAL_OFFSET, (unsigned long)S.instrument.Tipo_col,(unsigned long)EEP_CAL_OFFSET * (unsigned long)S.instrument.Tipo_col);			
	pnext0( (uint8_t *)S.unionFPOM.FPOMStruct.tmpBuf );


	readBuffersw( OFFSET+EEP_CAL_COD_OPERATORE,tmp,40); 	
	SPI_writeBuffersw( SPI_OLDCAL_COD_OPERATORE,tmp,40);
	readBuffersw( OFFSET+EEP_CAL_COD_LOCALITA,tmp,40); 	
	SPI_writeBuffersw( SPI_OLDCAL_COD_LOCALITA,tmp,40);
	readBuffersw( OFFSET+EEP_CAL_COD_IMPIANTO,tmp,40); 	
	SPI_writeBuffersw( SPI_OLDCAL_COD_IMPIANTO,tmp,40);
	readBuffersw( OFFSET+EEP_CAL_COD_P_PRELIEVO,tmp,40); 	
	SPI_writeBuffersw( SPI_OLDCAL_COD_P_PRELIEVO,tmp,40);
	readBuffersw( OFFSET+EEP_CAL_COD_ODORIZZANTE,tmp,40); 	
	SPI_writeBuffersw( SPI_OLDCAL_COD_ODORIZZANTE,tmp,40);
	
 	SPI_writeByte( SPI_OLDCAL_TIPO_COL,readByte( OFFSET+EEP_CAL_TIPO_COL ));
	SPI_writeByte( SPI_OLDCAL_ORA,readByte( OFFSET+EEP_CAL_ORA ));
	SPI_writeByte( SPI_OLDCAL_MINUTI,readByte( OFFSET+EEP_CAL_MINUTI ));
	SPI_writeByte( SPI_OLDCAL_GIORNO,readByte( OFFSET+EEP_CAL_GIORNO ));
	SPI_writeByte( SPI_OLDCAL_MESE,readByte( OFFSET+EEP_CAL_MESE ));
	SPI_writeByte( SPI_OLDCAL_ANNO,readByte( OFFSET+EEP_CAL_ANNO)); 
	SPI_writeFloat( SPI_OLDCAL_TEMP,S.instrument.Temp_Col );
	SPI_writeByte( SPI_OLDCAL_UNITS,readByte( OFFSET+EEP_CAL_UNITS ));  
	SPI_writeByte( SPI_OLDCAL_PICCHI,readByte( OFFSET+EEP_CAL_PICCHI));
	SPI_writeFloat( SPI_OLDCAL_GASFLOW,readFloat( OFFSET+EEP_CAL_GASFLOW)); 
	SPI_writeFloat( SPI_OLDCAL_PUMPFLOW,readFloat( OFFSET+EEP_CAL_PUMPFLOW));
	SPI_writeByte( SPI_OLDCAL_VIDEOGAIN,readByte( OFFSET+EEP_CAL_VIDEOGAIN )); 
	SPI_writeByte( SPI_OLDCAL_GAIN,readByte( OFFSET+EEP_CAL_GAIN )); 
	SPI_writeWord( SPI_OLDCAL_SLOPE,readWord( OFFSET+EEP_CAL_SLOPE )); 
	SPI_writeWord( SPI_OLDCAL_RES,readWord( OFFSET+EEP_CAL_RES ));
	SPI_writeByte( SPI_OLDCAL_NUM,readByte( OFFSET+EEP_CAL_NUM));
	
	SPI_WritePage (page);
	 resetWatchDog();

	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, "PRIMA PAGINA QQ : %lu \r\n",page);			
	pnext0( (uint8_t *)S.unionFPOM.FPOMStruct.tmpBuf );
	
for (i=0;i<5;i++){
	SPI_ClearBuf();
	
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, "PRIMA PAGINA CAL  : %lu \r\n",page);			
	pnext0( (uint8_t *)S.unionFPOM.FPOMStruct.tmpBuf );
	
	page= (SPI_OLDCAL_OFFSET * S.instrument.Cal_point) +1;
	
	SPI_writeLong( SPI_OLDCAL_ID,readLong( OFFSET+EEP_CAL_ID+(EEP_CAL_OFFSET_DATA*(i))	)); 
	SPI_writeLong(SPI_OLDCAL_AREA,readLong( OFFSET+EEP_CAL_AREA+(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeFloat( SPI_OLDCAL_CONC,readFloat( OFFSET+EEP_CAL_CONC+(EEP_CAL_OFFSET_DATA*(i))));
	SPI_writeWord( SPI_OLDCAL_INTENABLE,readWord( OFFSET+EEP_CAL_INTENABLE +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_INTDISABLE,readWord( OFFSET+EEP_CAL_INTDISABLE +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_AUTOZERO,readWord( OFFSET+EEP_CAL_AUTOZERO +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_DURATA_AZ,readWord( OFFSET+EEP_CAL_DURATA_AZ +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeFloat(SPI_OLDCAL_AZ_VALUE,readFloat(OFFSET+EEP_CAL_AZ_VALUE+(EEP_CAL_OFFSET_DATA*(i))));
	SPI_writeWord( SPI_OLDCAL_DURATA,readWord( OFFSET+EEP_CAL_DURATA )); 
 	resetWatchDog();
	
	
	readBuffersw( OFFSET+EEP_CAL_PICCO1+(EEP_CAL_OFFSET_DATA*(i)),tmp,5); 		
	SPI_writeBuffersw( SPI_OLDCAL_PICCO1,tmp,5 ); 
	SPI_writeLong( SPI_OLDCAL_AREA1,	  readLong( OFFSET+EEP_CAL_AREA1 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_INIZIO1,	  readWord( OFFSET+EEP_CAL_INIZIO1 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_MASSIMO1,   readWord( OFFSET+EEP_CAL_MASSIMO1 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_FINE1,	  readWord( OFFSET+EEP_CAL_FINE1+(EEP_CAL_OFFSET_DATA*(i))) ); 
	resetWatchDog();

	readBuffersw( OFFSET+EEP_CAL_PICCO2+(EEP_CAL_OFFSET_DATA*(i)),tmp,5); 		
	SPI_writeBuffersw( SPI_OLDCAL_PICCO2,tmp,5 ); 
	SPI_writeLong( SPI_OLDCAL_AREA2,	  readLong( OFFSET+EEP_CAL_AREA2 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_INIZIO2,	  readWord( OFFSET+EEP_CAL_INIZIO2 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_MASSIMO2,   readWord( OFFSET+EEP_CAL_MASSIMO2 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_FINE2,	  readWord( OFFSET+EEP_CAL_FINE2+(EEP_CAL_OFFSET_DATA*(i))) ); 
	resetWatchDog();

	readBuffersw( OFFSET+EEP_CAL_PICCO3+(EEP_CAL_OFFSET_DATA*(i)),tmp,5); 		
	SPI_writeBuffersw( SPI_OLDCAL_PICCO3,tmp,5 ); 
	SPI_writeLong( SPI_OLDCAL_AREA3,	  readLong( OFFSET+EEP_CAL_AREA3 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_INIZIO3,	  readWord( OFFSET+EEP_CAL_INIZIO3 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_MASSIMO3,   readWord( OFFSET+EEP_CAL_MASSIMO3 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_FINE3,	  readWord( OFFSET+EEP_CAL_FINE3+(EEP_CAL_OFFSET_DATA*(i))) ); 
	resetWatchDog();

	readBuffersw( OFFSET+EEP_CAL_PICCO4+(EEP_CAL_OFFSET_DATA*(i)),tmp,5); 		
	SPI_writeBuffersw( SPI_OLDCAL_PICCO4,tmp,5 ); 
	SPI_writeLong( SPI_OLDCAL_AREA4,	  readLong( OFFSET+EEP_CAL_AREA4 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_INIZIO4,	  readWord( OFFSET+EEP_CAL_INIZIO4 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_MASSIMO4,   readWord( OFFSET+EEP_CAL_MASSIMO4 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_FINE4,	  readWord( OFFSET+EEP_CAL_FINE4+(EEP_CAL_OFFSET_DATA*(i))) ); 
	resetWatchDog();

	readBuffersw( OFFSET+EEP_CAL_PICCO5+(EEP_CAL_OFFSET_DATA*(i)),tmp,5); 		
	SPI_writeBuffersw( SPI_OLDCAL_PICCO5,tmp,5 ); 
	SPI_writeLong( SPI_OLDCAL_AREA5,	  readLong( OFFSET+EEP_CAL_AREA5 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_INIZIO5,	  readWord( OFFSET+EEP_CAL_INIZIO5 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_MASSIMO5,   readWord( OFFSET+EEP_CAL_MASSIMO5 +(EEP_CAL_OFFSET_DATA*(i)))); 
	SPI_writeWord( SPI_OLDCAL_FINE5,	  readWord( OFFSET+EEP_CAL_FINE5+(EEP_CAL_OFFSET_DATA*(i))) ); 
	resetWatchDog();


	SPI_WritePage (page);
 	resetWatchDog();

	SPI_ClearBuf();

	for (j=0;j<264;j++){ resetWatchDog();
		SPI_writeWord( (j*2),(readWord( OFFSET+EEP_CAL_START_POINT+(EEP_CAL_OFFSET_DATA*(i))+(j*2))));
		}
	
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, "PRIMA PAGINA DATI CAL (%hu) : %lu %lu \r\n",i,page, 99999999);			
	pnext0( (uint8_t *)S.unionFPOM.FPOMStruct.tmpBuf );
	
	page++;
	
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, "PRIMA PAGINA DATI CAL (%hu) : %lu %lu \r\n",i,page,99999999);			
	pnext0( (uint8_t *)S.unionFPOM.FPOMStruct.tmpBuf );
	SPI_WritePage (page);

	SPI_ClearBuf();

	for (j=0;j<264;j++){ resetWatchDog();
		SPI_writeWord( (j*2),(readWord( OFFSET+EEP_CAL_START_POINT+(EEP_CAL_OFFSET_DATA*(i))+ ((j+264)*2+264))));
		}

	page++;
	SPI_WritePage (page);

	SPI_ClearBuf();

	for (i=0;i<264;i++){ resetWatchDog();
		SPI_writeWord( (j*2),(readWord( OFFSET+EEP_CAL_START_POINT+(EEP_CAL_OFFSET_DATA*(i))+ ((j+264)*2+528))));
		}
	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();

	for (i=0;i<264;i++){ resetWatchDog();
		SPI_writeWord( (j*2),(readWord( OFFSET+EEP_CAL_START_POINT+(EEP_CAL_OFFSET_DATA*(i))+ ((j+264)*2+792))));
		}

	page++;
	
	SPI_WritePage (page);
	
	SPI_ClearBuf();

	for (i=0;i<264;i++){ resetWatchDog();
		SPI_writeWord( (j*2),(readWord( OFFSET+EEP_CAL_START_POINT+(EEP_CAL_OFFSET_DATA*(i))+ ((j+264)*2+1056))));
		}
	page++;
	
	SPI_WritePage (page);

		SPI_ClearBuf();

	for (i=0;i<180;i++){ resetWatchDog();
		SPI_writeWord( (j*2),(readWord( OFFSET+EEP_CAL_START_POINT+(EEP_CAL_OFFSET_DATA*(i))+ ((j+264)*2+1320))));
		}
	page++;
	
	SPI_WritePage (page);
	SPI_ClearBuf();
  
}



S.instrument.Cal_point++;

//aggiorno in eeprom il numero di misure effettuate
writeWord( EEP_CAL_POINTER,S.instrument.Cal_point ); }

else {
	//KeyFunction((char *) txt_menu_exit);
        KeyFunction( (char *)txt_menu_OUT,(char *)txt_menu_FREE,(char *)txt_menu_FREE,(char *)txt_menu_FREE);   
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf,txt_Mem_Back_p);
	LCD_centerMessageBuf(Arial_10_BOLD, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	while (BUTTON_2==0){
		resetWatchDog();
		BUTTON_2=0;
		break;}
	
	
	}
       */
}


//carico misura da dataflash
void CaricaMisura(unsigned int numero){
/*
	unsigned int idx;


	unsigned long page;
 
 	resetWatchDog();
	page= ((SPI_MEAS_OFFSET * (numero-1))+SPI_MEAS_PAGE_INIT);
	S.Measure.Duration=SPI_readWord( SPI_MEAS_DURATA,page );

	S.Measure.Temp_Col	=SPI_readFloat( SPI_MEAS_TEMP,page);
	S.Measure.ID		=(unsigned long)SPI_readLong( SPI_MEAS_ID,page); 
	S.Measure.Area		=SPI_readLong( SPI_MEAS_AREA,page); 
	S.Measure.Concentrazione	=SPI_readFloat(SPI_MEAS_CONC,page); 
	S.Measure.Gasflow	=SPI_readFloat( SPI_MEAS_GASFLOW,page); 
	S.Measure.Pumpflow	=SPI_readFloat( SPI_MEAS_PUMPFLOW,page);
	S.Measure.Units		=SPI_readByte( SPI_MEAS_UNITS,page); 
	S.Measure.Videogain	=SPI_readByte( SPI_MEAS_VIDEOGAIN,page); 
	S.Measure.Gain		=SPI_readByte( SPI_MEAS_GAIN,page); 
	S.Measure.Picchi	=SPI_readByte( SPI_MEAS_PICCHI,page); 
 	resetWatchDog();
	S.Measure.Slope		=SPI_readWord( SPI_MEAS_SLOPE,page); 
	S.Measure.Res		=SPI_readWord( SPI_MEAS_RES,page); 
	S.Measure.Intenable	=SPI_readWord( SPI_MEAS_INTENABLE,page); 
	S.Measure.Intdisable	=SPI_readWord( SPI_MEAS_INTDISABLE,page); 
	S.Measure.duration	=SPI_readWord( SPI_MEAS_DURATA,page); 
	S.Measure.Autozero	=SPI_readWord( SPI_MEAS_AUTOZERO,page); 
	S.Measure.durationAutozero	=SPI_readWord( SPI_MEAS_DURATA_AZ,page); 
	S.Measure.AutoZeroValue		=SPI_readFloat( SPI_MEAS_AZ_VALUE,page);

	S.Measure.Ora		=SPI_readByte( SPI_MEAS_ORA,page);
	S.Measure.Minuti	=SPI_readByte( SPI_MEAS_MINUTI,page);
	S.Measure.Giorno	=SPI_readByte( SPI_MEAS_GIORNO,page);
	S.Measure.Mese		=SPI_readByte( SPI_MEAS_MESE,page);
	S.Measure.Anno		=SPI_readByte( SPI_MEAS_ANNO,page); 

	S.Measure.Cal_Area=				SPI_readLong( SPI_MEAS_CAL_AREA,page); 
	S.Measure.Cal_Concentrazione=	SPI_readFloat(SPI_MEAS_CAL_CONC,page); 
	S.Measure.Cal_Ora=				SPI_readByte( SPI_MEAS_CAL_ORA,page);
	S.Measure.Cal_Minuti=			SPI_readByte( SPI_MEAS_CAL_MINUTI,page);
	S.Measure.Cal_Giorno=			SPI_readByte( SPI_MEAS_CAL_GIORNO,page);
	S.Measure.Cal_Mese=				SPI_readByte( SPI_MEAS_CAL_MESE,page);
	S.Measure.Cal_Anno=				SPI_readByte( SPI_MEAS_CAL_ANNO,page); 

 resetWatchDog();
	
	SPI_readBuffersw( SPI_MEAS_COD_OPERATORE, (uint16_t)page, (uint8_t *)S.Measure.cod_operatore, 40);  
	SPI_readBuffersw( SPI_MEAS_COD_LOCALITA, page, 	(uint8_t *)S.Measure.cod_localita, 40);  
	SPI_readBuffersw( SPI_MEAS_COD_IMPIANTO, page, 	(uint8_t *)S.Measure.cod_impianto, 40);  
	SPI_readBuffersw( SPI_MEAS_COD_P_PRELIEVO, page, (uint8_t *)S.Measure.cod_p_prelievo, 40);  
	SPI_readBuffersw( SPI_MEAS_COD_ODORIZZANTE, page, (uint8_t *)S.Measure.cod_odorizzante, 40);  


	SPI_readBuffersw( SPI_MEAS_PICCO1,page, (uint8_t *)S.Measure.Picco[0].nome_picco, 5); 
	
	S.Measure.Picco[0].area 	= SPI_readLong( SPI_MEAS_AREA1,page); 
	S.Measure.Picco[0].inizio 	= SPI_readWord( SPI_MEAS_INIZIO1,page); 
	S.Measure.Picco[0].massimo 	= SPI_readWord( SPI_MEAS_MASSIMO1,page); 
	S.Measure.Picco[0].fine 	= SPI_readWord( SPI_MEAS_FINE1,page); 

	SPI_readBuffersw( SPI_MEAS_PICCO2,page, (uint8_t *)S.Measure.Picco[1].nome_picco, 5); 
	S.Measure.Picco[1].area 	= SPI_readLong( SPI_MEAS_AREA2,page); 
	S.Measure.Picco[1].inizio 	= SPI_readWord( SPI_MEAS_INIZIO2,page); 
	S.Measure.Picco[1].massimo 	= SPI_readWord( SPI_MEAS_MASSIMO2,page); 
	S.Measure.Picco[1].fine 	= SPI_readWord( SPI_MEAS_FINE2,page); 
 	resetWatchDog();

	SPI_readBuffersw( SPI_MEAS_PICCO3,page, (uint8_t *)S.Measure.Picco[2].nome_picco, 5); 
	S.Measure.Picco[2].area 	= SPI_readLong( SPI_MEAS_AREA3,page); 
	S.Measure.Picco[2].inizio 	= SPI_readWord( SPI_MEAS_INIZIO3,page); 
	S.Measure.Picco[2].massimo 	= SPI_readWord( SPI_MEAS_MASSIMO3,page); 
	S.Measure.Picco[2].fine 	= SPI_readWord( SPI_MEAS_FINE3,page); 

	SPI_readBuffersw( SPI_MEAS_PICCO4,page, (uint8_t *)S.Measure.Picco[3].nome_picco, 5); 
	S.Measure.Picco[3].area 	= SPI_readLong( SPI_MEAS_AREA4,page); 
	S.Measure.Picco[3].inizio 	= SPI_readWord( SPI_MEAS_INIZIO4,page); 
	S.Measure.Picco[3].massimo 	= SPI_readWord( SPI_MEAS_MASSIMO4,page); 
	S.Measure.Picco[3].fine 	= SPI_readWord( SPI_MEAS_FINE4,page); 

	SPI_readBuffersw( SPI_MEAS_PICCO5,page, (uint8_t *)S.Measure.Picco[4].nome_picco, 5); 
	S.Measure.Picco[4].area 	= SPI_readLong( SPI_MEAS_AREA5,page); 
	S.Measure.Picco[4].inizio 	= SPI_readWord( SPI_MEAS_INIZIO5,page); 
	S.Measure.Picco[4].massimo 	= SPI_readWord( SPI_MEAS_MASSIMO5,page); 
	S.Measure.Picco[4].fine 	= SPI_readWord( SPI_MEAS_FINE5,page); 
	page++;

	for (idx=0;idx<264;idx++){ 
		resetWatchDog();
		S.Measure.Data[idx]=SPI_readWord((idx*2),page);
		}
	page++;

	for (idx=0;idx<264;idx++){ 
		resetWatchDog();
		S.Measure.Data[idx+264]=SPI_readWord((idx*2),page);
		}
	page++;
	
		for (idx=0;idx<264;idx++){ 
		resetWatchDog();
		S.Measure.Data[idx+528]=SPI_readWord((idx*2),page);
		}
	page++;
	
		for (idx=0;idx<264;idx++){ 
		resetWatchDog();
		S.Measure.Data[idx+792]=SPI_readWord((idx*2),page);
		}
	page++;
	
		for (idx=0;idx<264;idx++){ 
		resetWatchDog();
		S.Measure.Data[idx+1056]=SPI_readWord((idx*2),page);
		}
	page++;
	
		for (idx=0;idx<180;idx++){ 
		resetWatchDog();
		S.Measure.Data[idx+1320]=SPI_readWord((idx*2),page);
		}
            
}

//metto a 0 le variabili usate per la misura
void ResetVar(void){

unsigned int i;
S.Measure.Picco[0].inizio=0;
S.Measure.Picco[1].inizio=0;
S.Measure.Picco[2].inizio=0;
S.Measure.Picco[3].inizio=0;
S.Measure.Picco[4].inizio=0;
S.Measure.Picco[0].fine=0;
S.Measure.Picco[1].fine=0;
S.Measure.Picco[2].fine=0;
S.Measure.Picco[3].fine=0;
S.Measure.Picco[4].fine=0;
S.Measure.Picco[0].area=0;
S.Measure.Picco[1].area=0;
S.Measure.Picco[2].area=0;
S.Measure.Picco[3].area=0;
S.Measure.Picco[4].area=0;
S.Measure.Picco[0].massimo=0;
S.Measure.Picco[1].massimo=0;
S.Measure.Picco[2].massimo=0;
S.Measure.Picco[3].massimo=0;
S.Measure.Picco[4].massimo=0;
S.Measure.Area=0;
S.Measure.Concentrazione=0;
S.Measure.AutoZeroValue=0;

  */
}

//faccio la misura sui dati precedentemente acquisiti
void doMeasureCalculation(unsigned char OU, unsigned char OD)
{

	unsigned int i, Texc;
	const int minimo_picco=2;

	LCD_Clear();
        /********************************/
        /*ridiscegno il grafico         */
       /********************************/
	refresh(S.instrument.duration-1);

ricalc:
	S.instrument.passoH=(((float)(GDISPH-(OD+OU))/0x3ff)*(float)S.instrument.videogain);//0.0919;
	S.measure.peakStart=0;
	S.measure.peakStop=0;
	S.measure.peakMax=0;
	S.measure.peakArea=0;
        
	S.measure.area=0;
	S.measure.concentration=0;

//Calcola l'autozero
        S.instrument.autoZeroValue=0;
        if (S.instrument.durationAutozero<3){
            S.instrument.durationAutozero=3;
	}
 //if ((S.instrument.Autozero+S.instrument.durationAutozero)>S.instrument.Intenable){
 //	LCD_Clear();
 //	sprintf( tmpBuf, txt_AZ_Err);
 //	centerMessageBuf( tmpBuf);
 //	goto ex;
 //	}
	
if (S.instrument.intEnable>S.instrument.duration){
	LCD_Clear();
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, txt_AZ_e_err[S.unionFPOM.FPOMStruct.curLanguage]);
        LCD_String (Arial_10_BOLD,  10, 60, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	goto ex;
	}
        
if (S.measure.intDisable>S.measure.duration){
	LCD_Clear();
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, txt_AZ_d_err[S.unionFPOM.FPOMStruct.curLanguage]);
        LCD_String (Arial_10_BOLD,  10, 60, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	goto ex;
	}
        
if (S.measure.intEnable>S.measure.intDisable){
	LCD_Clear();
	sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, txt_P_err[S.unionFPOM.FPOMStruct.curLanguage]);
        LCD_String (Arial_10_BOLD,  10, 60, (char *)S.unionFPOM.FPOMStruct.tmpBuf);
	goto ex;
	}

  for (i=(S.instrument.autoZeroStart);i<=(S.instrument.autoZeroStart+S.instrument.durationAutozero);i++){
	S.instrument.autoZeroValue=S.instrument.autoZeroValue+S.instrument.data[i];
  }
S.instrument.autoZeroValue=S.instrument.autoZeroValue/(S.instrument.durationAutozero+1);

LCD_Gi_line( 0, (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroValue))+OD)), 
            GDISPW, (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroValue))+OD)),0);

LCD_Gi_line( ((unsigned int)(((float) 240/S.instrument.duration)*S.instrument.autoZeroStart)), 
            (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroStart))+OD)),
             ((unsigned int)(((float) 240/S.instrument.duration)*S.instrument.autoZeroStart)), 
             (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroStart))+OD+25)),0);

LCD_Gi_line( ((unsigned int)(((float) 240/S.instrument.duration)*(S.instrument.autoZeroStart+S.instrument.durationAutozero))), 
              (127-((unsigned int)(S.instrument.passoH*(S.measure.autoZeroValue))+OD)),
              ((unsigned int)(((float) 240/S.instrument.duration)*(S.instrument.autoZeroStart+S.instrument.durationAutozero))),
              (127-((unsigned int)(S.instrument.passoH*(S.measure.autoZeroStart))+OD+25)),0);



//Cerco il massimo
S.measure.peakMax=0;

for (i=S.instrument.intEnable;i<S.instrument.intDisable;i++){
	if (S.instrument.data[i-1]>S.instrument.data[S.measure.peakMax]){
		S.measure.peakMax=i;}

if (S.measure.peakMax==0)
{break;}



//*****************************************************************************************************

//cerco inizio del picco
Texc=0;
S.measure.peakStart=S.measure.peakMax;


for (i=S.measure.peakMax;i>S.instrument.intEnable; i--){
	if ((S.instrument.data[i])<S.instrument.data[S.measure.peakStart]){
		if (S.instrument.data[i]>=S.instrument.autoZeroValue){
		S.measure.peakStart=i;
		Texc=0;}}
	else {Texc++;}
	if ((S.instrument.data[i]<S.instrument.autoZeroValue)){
		break;}
	if((Texc>S.measure.slope)&&((S.instrument.data[i]-S.instrument.autoZeroValue)<=((S.instrument.data[S.measure.peakMax]-S.measure.autoZeroValue)/100*30))){
		S.measure.peakStart=i+Texc;
		break;}
}


//*****************************************************************************************************


//*****************************************************************************************************
//cerco fine del picco
Texc=0;
S.measure.peakStop=S.measure.peakMax;


for (i=S.measure.peakMax;i<S.instrument.intDisable; i++){
	if ((S.instrument.data[i])<S.instrument.data[S.measure.peakStop]){
		if (S.instrument.data[i]>=S.instrument.autoZeroValue){
		S.measure.peakStop=i;
		Texc=0;}}
	else {Texc++;}
	if ((S.instrument.data[i]<S.instrument.autoZeroValue)){
		break;}
	if((Texc>S.instrument.slope)&&((S.instrument.data[i]-S.instrument.autoZeroValue)<=((S.instrument.data[S.measure.peakMax]-S.instrument.autoZeroValue)/100*30))){
		S.measure.peakStop=i-Texc;
		break;
        }
}


//*****************************************************************************************************
 VISUAL_GRATICULE(OFFU,OFFD);


//Disegno linee di inizio, massimo fine e evidenzio e calcolo l'area
if (S.measure.peakStop-S.measure.peakStart>=minimo_picco){
	LCD_Gi_line( ((unsigned int)(((float) 240/S.instrument.duration)*S.measure.peakMax)),  30, ((unsigned int)(((float) 240/S.instrument.duration)*S.measure.peakMax
)),  (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroValue))+OD)) ,0);
	LCD_Gi_line( ((unsigned int)(((float) 240/S.instrument.duration)*S.measure.peakStart)),  40, ((unsigned int)(((float) 240/S.instrument.duration)*S.measure.peakStart
)),  (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroValue))+OD)) ,0);
	LCD_Gi_line( ((unsigned int)(((float) 240/S.instrument.duration)*S.measure.peakStop)),  40, ((unsigned int)(((float) 240/S.instrument.duration)*S.measure.peakStop
)),  (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroValue))+OD)) ,0);

	//calcolo rapporto tra i diversi guadagni, solo se calibrazione
	if (S.instrument.tipo_analisi==1){
		Rapporto_Gain();}

	for (i=S.measure.peakStart;i<=S.measure.peakStop;i++){	
		LCD_Gi_line( ((unsigned int)(i*((float) 240/S.measure.duration))), 
                            (127-((unsigned int)(S.instrument.passoH*(S.instrument.autoZeroValue))+OD)), 
                            ((unsigned int)(i*((float) 240/S.measure.duration))),
                            (127-((unsigned int)(S.instrument.passoH*(S.instrument.data[i]))+OD)) ,0);
		if (S.instrument.data[i]>=S.instrument.autoZeroValue){
		S.measure.peakArea=((unsigned long)S.measure.peakArea+((S.instrument.data[i])-(unsigned long)S.instrument.autoZeroValue));}
	}

	if (S.instrument.meas_gain==1){
		S.measure.peakArea=(unsigned long)(((float)S.measure.peakArea)*((float)S.instrument.rapporto_Gain));		
	}

//if ((S.instrument.meas_gain==1)&&(S.instrument.cal_gain==2)/*&&(S.instrument.tipo_analisi!=1)){
//	S.measure.Picco[picco].area=(unsigned long)((float)S.measure.Picco[picco].area)*((float)S.instrument.Rapporto_Gain);}
//if ((S.instrument.meas_gain==2)&&(S.instrument.cal_gain==1)/*&&(S.instrument.tipo_analisi!=1)){
//	S.measure.Picco[picco].area=(unsigned long)((float)S.measure.Picco[picco].area)/((float)S.instrument.Rapporto_Gain);}

}
}

ex:
	//settaggio parametri di misura:
	S.measure.id=		S.instrument.id;
	S.measure.temp_Col=	S.instrument.temp_Col;
	//S.measure.gasflow=	S.instrument.gasflow;
	//S.measure.pumpflow=	S.instrument.pumpflow;
	S.measure.videogain=    S.instrument.videogain;
	S.measure.gain=		S.instrument.gain;
	S.measure.slope=	S.instrument.slope;
	S.measure.resolution=	S.instrument.resolution;
	S.measure.intEnable=    S.instrument.intEnable;
	S.measure.intDisable=   S.instrument.intDisable;
	S.measure.duration=	S.instrument.duration;
	S.measure.autoZeroValue=	S.instrument.autoZeroValue;
	S.measure.durationAutozero=     S.instrument.durationAutozero;

	S.measure.hour		=S.unionFPOM.FPOMStruct.orologio.sysHour;
	S.measure.minutes		=S.unionFPOM.FPOMStruct.orologio.sysMin;
	S.measure.day		=S.unionFPOM.FPOMStruct.orologio.sysDay;
	S.measure.month		=S.unionFPOM.FPOMStruct.orologio.sysMonth;
	S.measure.year		=S.unionFPOM.FPOMStruct.orologio.sysYear; 
	//S.measure.Cal_Area	=S.instrument.Area;
	//S.measure.Cal_Concentrazione	=S.instrument.Concentrazione;

	S.measure.calHour	=(readByte( EEP_CAL_ORA));
	S.measure.calMinutes     =(readByte( EEP_CAL_MINUTI ));
	S.measure.calDay         =(readByte( EEP_CAL_GIORNO ));
	S.measure.calMonth	=(readByte( EEP_CAL_MESE ));
	S.measure.calyear	=(readByte( EEP_CAL_ANNO ));
        
	//vengono ordinati i picchi e calcolata l'area
        
        S.instrument.rapporto_Gain=((float)S.measure.area/S.measure.concentration);
        S.measure.concentration=((float)S.measure.area/S.instrument.rapporto_Gain);
        S.measure.calArea		=S.measure.area;
        S.measure.calConcentration	=S.measure.concentration;




	if (S.instrument.tipo_analisi!=2){
                 KeyFunction( (char *)txt_menu_OUT[S.unionFPOM.FPOMStruct.curLanguage],(char *)txt_menu_PARAM[S.unionFPOM.FPOMStruct.curLanguage],(char *)txt_menu_GUAD[S.unionFPOM.FPOMStruct.curLanguage],(char *)txt_menu_SAVE[S.unionFPOM.FPOMStruct.curLanguage]);
		if (S.instrument.tipo_analisi==0){

			sprintf( (char *)S.unionFPOM.FPOMStruct.tmpBuf, "%04.2fmg   %lu  ",(S.measure.concentration) ,(S.measure.area));
                        LCD_String (Arial_10_BOLD,  3, 52, (char *)S.unionFPOM.FPOMStruct.tmpBuf);

			}
			while (1){

			if (BUTTON_2==1){
				BUTTON_2=0;
				ptrMenu = menu_ParamMeasChange;
				menu_ParamMeasChange();
			
			while ( ptrMenu!=menu_Idle){
				//if(tim1msExecuteMenu == 0) {
               		//tim1msExecuteMenu = 10;
                	( *ptrMenu ) ();
				//}
			}
	
			refresh(S.measure.duration-1);
		goto ricalc;
		}

		if(BUTTON_3==1){
			BUTTON_3=0;
			change_gain();
			refresh(S.measure.duration-1);
			goto ricalc;
		}

		if ((BUTTON_1==1)|(BUTTON_4==1))
			{
                          BUTTON_1 = 0;
                          BUTTON_4 = 0;
                          return;
                        }	
		}
	}
}


//server per passare dall alta amplificazione a quella bassa
void change_gain(void){

  if (S.instrument.gain==2){
     S.instrument.gain=1;
       if (S.instrument.tipo_analisi==1){
          S.instrument.cal_gain=1;
	  S.instrument.gain=S.instrument.cal_gain;
       }
  } else{
	if (S.instrument.gain==1){
		S.instrument.meas_gain=2;
		S.instrument.gain=S.instrument.meas_gain;
        } else {
            if (S.instrument.gain==2){
		S.instrument.meas_gain=1;
		S.instrument.gain=S.instrument.meas_gain;
            }
        }		

    }
 }

//calcolo rapporto tra i 2 ingressi analogici
void Rapporto_Gain (void){
	unsigned int i;
	unsigned long area1, area2;
	area1=0;
	area2=0;
	for (i=0;i<S.instrument.duration;i++){
		if ((S.instrument.data[i]>=0x3ff)||(S.instrument.data2[i]>=0x3ff))
			{break;}
		area1=area1+S.instrument.data[i];
		area2=area2+S.instrument.data2[i];
	}
	
	if (i==S.instrument.duration){
		S.instrument.rapporto_Gain=(float)area2/(float)area1;
		writeFloat(EEP_RAPP_GAIN,S.instrument.rapporto_Gain );
	}

}


//******************************************
//gestisco il riscaldamentodella colonna
//******************************************

void Riscaldamento(){
    S.instrument.temp1= readTemperature(6);
    S.instrument.temp2= readTemperature(7);
    S.instrument.tcolnow=((S.instrument.temp1+S.instrument.temp2)/2);
    if (S.instrument.tcolnow>(S.instrument.temp_Col+1)){
      setPWMHeath(0);
      PWM_Start();
    }else{
      if (S.instrument.tcolnow<(S.instrument.temp_Col-10)){
        setPWMHeath(200);
        PWM_Start();
      } else if (S.instrument.tcolnow<(S.instrument.temp_Col-4)){
        setPWMHeath(120);
        PWM_Start();
      } else if (S.instrument.tcolnow<(S.instrument.temp_Col-1)){
        setPWMHeath(50);
        PWM_Start();
      } else if (S.instrument.tcolnow<(S.instrument.temp_Col)){
        setPWMHeath(20);
        PWM_Start();
      }
    
    }
}


//Cambio valore del PWM della pompa, accesa - spenta
void SetPompaFlow(char stato){
  if(S.instrument.pumpSetPower>100){
    S.instrument.pumpSetPower=100;
  }
    
  if(S.instrument.pumpSetPower<0){
    S.instrument.pumpSetPower=0;
  }
    
  setPumpFlow((unsigned char)S.instrument.pumpSetPower );
  if (stato==1){
		    
     HAL_GPIO_WritePin(GPIOE, PUMP_CTRL_P_Pin, GPIO_PIN_RESET);
		}
  else if (stato==0){

    HAL_GPIO_WritePin(GPIOE, PUMP_CTRL_P_Pin, GPIO_PIN_SET);
		
  }
}




  
  
    
                  
  

