#include "stm32l4xx_hal.h"
#include "ad.h"
#include "sensit.h"
#include <stdint.h>
//#include "taskManager.h"

// Vettori gestiti in automatico dal DMA associato agli ADC1 e ADC2
uint16_t ADC1ConvertedValues[16];
uint16_t ADC2ConvertedValues[16];

// Definizione delle dimensione dei buffer associati ai canali AD
#define DIM_V_Temp              16
#define DIM_LASER_ANODE_BUF     16
#define DIM_LASER_CATHODE_BUF   16
#define DIM_difoutBUF           16

#define DIM_IPELT               16
#define DIM_TEMP_LASER          16 


// Vettori riservati per il calcolo della media
uint16_t  buf_V_Temp[DIM_V_Temp];                       
uint16_t  buf_LASER_ANODE_BUF[DIM_LASER_ANODE_BUF];     
uint16_t  buf_LASER_CATHODE_BUF[DIM_LASER_CATHODE_BUF]; 
uint16_t  buf_difoutBUF[DIM_difoutBUF];   

uint16_t  buf_IPELT[DIM_IPELT];                           
uint16_t  buf_TEMP_LASER[DIM_TEMP_LASER];                   
 

// valori della media
uint16_t  avg_ad_V_Temp;                        
uint16_t  avg_ad_LASER_ANODE_BUF;      
uint16_t  avg_ad_LASER_CATHODE_BUF;  
uint16_t  avg_ad_difoutBUF;   

uint16_t  avg_ad_IPELT;                            
uint16_t  avg_ad_TEMP_LASER;                    
                 


// Strutture con parametri gestione buffer circolare e calcolo  della media
type_adStruct  ad_V_Temp;                        
type_adStruct  ad_LASER_ANODE_BUF;      
type_adStruct  ad_LASER_CATHODE_BUF;  
type_adStruct  ad_difoutBUF;                  
type_adStruct  ad_IPELT;                            
type_adStruct  ad_TEMP_LASER;                    
            


void initAdStruct(type_adStruct *adStr, uint16_t ch, uint16_t * destBuf, uint16_t * srcBuf, uint16_t dimBuf)
{
  adStr->ch=ch;
  adStr->destBuf=destBuf;
  adStr->srcBuf=srcBuf;
  adStr->idxBuf=0;
  adStr->dimBuf=dimBuf;
  for(uint16_t i=0; i<dimBuf; ++i) {
    destBuf[i]=srcBuf[ch];
  }
  adStr->avg=srcBuf[ch];
}


void putAdStruct(type_adStruct *adStr)
{
  adStr->destBuf[adStr->idxBuf++] = adStr->srcBuf[adStr->ch];

  if (adStr->idxBuf >= adStr->dimBuf)
    adStr->idxBuf=0;
}

uint16_t avgAdStruct(type_adStruct *adStr)
{
  uint32_t sumAdBuf=adStr->destBuf[0];
  
  for(uint16_t i=1; i<adStr->dimBuf; ++i)
    sumAdBuf += (uint32_t)adStr->destBuf[i];
  
  sumAdBuf /= (uint32_t)adStr->dimBuf;
  
  adStr->avg = (uint16_t)sumAdBuf;
  
  return (uint16_t)sumAdBuf;
}

/*
OLD
void initAnalogInputs ( void )
{
  initAdStruct(&ad_V_Temp,            0, buf_V_Temp,            ADC1ConvertedValues, DIM_V_Temp);
  initAdStruct(&ad_LASER_ANODE_BUF,   1, buf_LASER_ANODE_BUF,   ADC1ConvertedValues, DIM_LASER_ANODE_BUF);
  initAdStruct(&ad_LASER_CATHODE_BUF, 2, buf_LASER_CATHODE_BUF, ADC1ConvertedValues, DIM_LASER_CATHODE_BUF);
  initAdStruct(&ad_difoutBUF,         3, buf_difoutBUF,         ADC1ConvertedValues, DIM_difoutBUF);
  initAdStruct(&ad_ITEC,              4, buf_ITEC,              ADC1ConvertedValues, DIM_ITEC);

  initAdStruct(&ad_ADC2_IN8,          0, buf_ADC2_IN8,          ADC2ConvertedValues, DIM_ADC2_IN8);  
  initAdStruct(&ad_ADC2_IN10,         1, buf_ADC2_IN10,         ADC2ConvertedValues, DIM_ADC2_IN10);  
  initAdStruct(&ad_ADC2_IN11,         2, buf_ADC2_IN11,         ADC2ConvertedValues, DIM_ADC2_IN11);  
  initAdStruct(&ad_ADC2_IN12,         3, buf_ADC2_IN12,         ADC2ConvertedValues, DIM_ADC2_IN12);  
}
*/

void initAnalogInputs ( void )
{
  //
  initAdStruct(&ad_IPELT,             1, buf_IPELT,             ADC1ConvertedValues, DIM_IPELT);                // PC1 pin27
  initAdStruct(&ad_TEMP_LASER,        2, buf_TEMP_LASER,        ADC1ConvertedValues, DIM_TEMP_LASER);           // PC2 pin28
  initAdStruct(&ad_difoutBUF,         3, buf_difoutBUF,         ADC1ConvertedValues, DIM_difoutBUF);            // PC3 pin29
  initAdStruct(&ad_LASER_ANODE_BUF,   4, buf_LASER_ANODE_BUF,   ADC1ConvertedValues, DIM_LASER_ANODE_BUF);      // PA1 pin35
  initAdStruct(&ad_LASER_CATHODE_BUF, 5, buf_LASER_CATHODE_BUF, ADC1ConvertedValues, DIM_LASER_CATHODE_BUF);    // PA2 pin36

  initAdStruct(&ad_V_Temp,            0, buf_V_Temp,            ADC2ConvertedValues, DIM_V_Temp);               // PA3 pin37 - Temperatura esterna pin2.CN4
 
}

void updateAdStruct(void)
{   
  putAdStruct(&ad_LASER_ANODE_BUF);  
  putAdStruct(&ad_LASER_CATHODE_BUF);
  putAdStruct(&ad_V_Temp);        
  putAdStruct(&ad_difoutBUF);
  
  putAdStruct(&ad_IPELT);             
  putAdStruct(&ad_TEMP_LASER);         
      
}


void getAdAverageValue(void)
{
  avg_ad_LASER_ANODE_BUF=avgAdStruct(&ad_LASER_ANODE_BUF);
  avg_ad_LASER_CATHODE_BUF=avgAdStruct(&ad_LASER_CATHODE_BUF);
  avg_ad_V_Temp=avgAdStruct(&ad_V_Temp);  
  avg_ad_difoutBUF=avgAdStruct(&ad_difoutBUF);
  
  avg_ad_IPELT=avgAdStruct(&ad_IPELT);
  avg_ad_TEMP_LASER=avgAdStruct(&ad_TEMP_LASER);
}
