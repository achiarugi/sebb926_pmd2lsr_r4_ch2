/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "spi_adc_ltc1864l.h"
#include "vee.h"
#include "sensit.h"
#include "out.h"
#include "taskManager.h"
#include "i2c_digitalpot_AD5243.h"

/* Private variables ---------------------------------------------------------*/
extern SPI_HandleTypeDef hspi2;
extern DAC_HandleTypeDef hdac;
extern TIM_HandleTypeDef htim6;
extern 	uint16_t waveform[1024];
void enableINT(void);
void disableINT(void);

/**
  * @brief  The application entry point.
  *
  * @retval None
  */

static void spi_usleep ( unsigned int delusecs );

#ifdef __KEIL__
#pragma  push
#pragma  O0
#endif

HAL_StatusTypeDef ltc1864l_get_value(uint16_t *adc_value)
{
  //uint16_t dummy_buffer_tx;
  HAL_StatusTypeDef retVal;

  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  //SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin;
  spi_usleep(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
  //SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U;
  //retVal=HAL_SPI_Receive(&hspi2, (uint8_t *)  adc_value,1 , HAL_MAX_DELAY);
	retVal=HAL_SPI_Receive(&hspi2, (uint8_t *)  adc_value,1 , HAL_MAX_DELAY);
  
  return retVal;
//  uint16_t dummy_buffer_tx;
//  HAL_StatusTypeDef retVal;
//  
//  SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U;
//  
//  retVal=HAL_SPI_TransmitReceive(&hspi2, (uint8_t *) &dummy_buffer_tx, (uint8_t *)  adc_value,1 , HAL_MAX_DELAY);
//  
//  SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin;
//  
//  return retVal;
  
}
static void spi_usleep ( unsigned int delusecs )
{
    int i;
    for ( i = 0; i < 20 * delusecs; i++ );
}



#ifdef __KEIL__  
  if(interrupt_mask)
    __enable_irq();
#endif
      




#ifdef __KEIL__
#pragma  push
#pragma  O0
#endif
void read_rampa_DC(uint32_t *pvalue, uint16_t n_samples, uint16_t n_mean){
  uint16_t adc_value;
  uint32_t temp[512];
  uint8_t up, dw;
	uint16_t index_mean=0; 
	uint8_t count_out;
	uint16_t jumplevel;
	jumplevel=100;
	uint32_t background;
	uint16_t xmin, xmax;
	uint32_t ymin, ymax;

  for (uint16_t i=0; i<n_samples; i++){
    *(pvalue+i)=0;
		temp[i]=0;
  }
	//int interrupt_mask = __disable_irq();
	disableINT();
	count_out=0;
  while (index_mean<n_mean){
    //wait_for_trigger_counter();
    wait_for_trigger_micro();
		up=0; dw=0;
		fastLtc1864l_get_value(temp, n_samples);
		for (uint16_t i=0; i<n_samples; i++)
			temp[i]-=measurement.zerophd;
		background=measurement.amplResistorValue*(50.0/256.0)*measurement.dCLevelW;
		minmaxV(temp, measurement.nAcquisitionPointsW/2, &xmin, &ymin, &xmax, &ymax);		
		if (ymin<1000)
				dw=1;
		if (ymax>64536)
				up=1;

		if ((ymax-ymin)<32000 && (ymax-ymin)>100 ){
				measurement.amplResistorValue*=2;
				if (measurement.amplResistorValue>255)
					measurement.amplResistorValue=255;
				AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, measurement.amplResistorValue, 0);
				AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, measurement.amplResistorValue, 0);
		}
		if ((ymax-ymin)>63000){
				measurement.amplResistorValue/=2;
				if (measurement.amplResistorValue<2)
					measurement.amplResistorValue=2;
				AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, measurement.amplResistorValue, 0);
				AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, measurement.amplResistorValue, 0);
		}
  	if ((dw==0 && up==0)|| count_out>30 || measurement.autoBackground==0){
				for (uint16_t i=0; i<n_samples; i++)
					*(pvalue+i) +=temp[i]+background;
			index_mean++;
			measurement.rampStatus=0;
		}else if (dw==1 && up==0 ){
      if (measurement.dCLevelW<600){
				for (uint16_t i=0; i<n_samples; i++){
					*(pvalue+i) +=temp[i]+background;			
				}
				set_ad5693_DC_level(500);
				index_mean++;
			}else{
        measurement.dCLevelW=measurement.dCLevelW-jumplevel;
				jumplevel*=1.2;
				if (jumplevel>3000)
					jumplevel=3000;
        set_ad5693_DC_level(measurement.dCLevelW);
				count_out++;
			}
			measurement.rampStatus=1;
    }else if (dw==0 && up==1){
      if (measurement.dCLevelW>64900){
				for (uint16_t i=0; i<n_samples; i++){
					*(pvalue+i) +=temp[i]+background;
				}
				set_ad5693_DC_level(65000);
				index_mean++;
			}else{
        measurement.dCLevelW=measurement.dCLevelW+jumplevel;
				jumplevel*=1.2;
				if (jumplevel>3000)
					jumplevel=3000;
        set_ad5693_DC_level(measurement.dCLevelW);
				count_out++;
			}
			measurement.rampStatus=2;
    }else if(dw==1 && up==1){
			measurement.rampStatus=3;
			for (uint16_t i=0; i<n_samples; i++){
					*(pvalue+i) +=temp[i]+background;
      }
			index_mean++;
		}
	}
	enableINT();
}


#ifdef __KEIL__
#pragma pop
#endif

void wait_for_trigger_micro(void){

	while(global_triggerRampa)
		__asm("nop");
	while(!global_triggerRampa)
		__asm("nop");
	global_triggerRampa=0;
	spi_usleep(470+service.triggerRampa);
//	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)waveform, 1024, DAC_ALIGN_12B_R); 
}

void disableINT(void)
{
	//HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
	HAL_NVIC_DisableIRQ(SysTick_IRQn);
	HAL_NVIC_DisableIRQ(TIM1_BRK_TIM15_IRQn);
	HAL_NVIC_DisableIRQ(TIM1_UP_TIM16_IRQn);	
  HAL_NVIC_DisableIRQ(DMA1_Channel1_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel2_IRQn);
//  HAL_NVIC_DisableIRQ(DMA1_Channel3_IRQn);
//  HAL_NVIC_DisableIRQ(DMA1_Channel4_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel5_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel6_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel7_IRQn);
	HAL_NVIC_DisableIRQ(USART1_IRQn);
	HAL_NVIC_DisableIRQ(USART2_IRQn);
//	HAL_NVIC_DisableIRQ(TIM6_DAC_IRQn); 
}

void enableINT(void)
{
	//HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(SysTick_IRQn);
	HAL_NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn);
	HAL_NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);	
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
//  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
//  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
	HAL_NVIC_EnableIRQ(USART1_IRQn);
	HAL_NVIC_EnableIRQ(USART2_IRQn);
//	HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
	

}


#ifdef __KEIL__
#pragma  push
#pragma  O0
#endif
void  LTC1864L_SPI_Receive(uint8_t *pTxData, uint8_t *pRxData, uint32_t Timeout);

void fastLtc1864l_get_value( uint32_t *temp, uint16_t n_samples)
{
	uint16_t adc_value;
  uint32_t *ptrTemp;
  HAL_StatusTypeDef retVal;
	uint16_t dummyTX=0;

	SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1	
	disableINT();
	SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
	for (uint16_t i=0; i<n_samples; i++){
		//SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0		
		//WDI_GPIO_Port->ODR ^= WDI_Pin; //  for debug only

		//spi_usleep(1);
		//SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
		temp[i]=0.0;
		LTC1864L_SPI_Receive((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
	}
	enableINT();

/**********************************/		
}


void postProcessingLtc1864l_get_value( void)
{
}
#ifdef __KEIL__
#pragma  pop
#pragma  O0
#endif

extern HAL_StatusTypeDef SPI_WaitFlagStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State, uint32_t Timeout, uint32_t Tickstart);
extern HAL_StatusTypeDef SPI_WaitFifoStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Fifo, uint32_t State, uint32_t Timeout, uint32_t Tickstart);
extern HAL_StatusTypeDef SPI_EndRxTxTransaction(SPI_HandleTypeDef *hspi, uint32_t Timeout, uint32_t Tickstart);

inline void LTC1864L_SPI_Receive(uint8_t *pTxData, uint8_t *pRxData, uint32_t Timeout)
{
  uint32_t tickstart = 0U;

  /* Process Locked */
  //__HAL_LOCK(&hspi2); WARNING! LH 
	hspi2.Lock = HAL_LOCKED; // Locked forced see __HAL_LOCK macro
	
extern __IO uint32_t uwTick;
  /* Init tickstart for timeout management*/
  tickstart = uwTick; //HAL_GetTick();

  /* Don't overwrite in case of HAL_SPI_STATE_BUSY_RX */
  if (hspi2.State != HAL_SPI_STATE_BUSY_RX)
  {
    hspi2.State = HAL_SPI_STATE_BUSY_TX_RX;
  }

  /* Set the transaction information */
  hspi2.ErrorCode   = HAL_SPI_ERROR_NONE;
  //hspi2.pRxBuffPtr  = (uint8_t *)pRxData;
  //hspi2.RxXferCount = 1;
  //hspi2.RxXferSize  = 1;
  //hspi2.pTxBuffPtr  = (uint8_t *)pTxData;
  //hspi2.TxXferCount = 1;
  //hspi2.TxXferSize  = 1;

  /*Init field not used in handle to zero */
  //hspi2.RxISR       = NULL;
  //hspi2.TxISR       = NULL;

  /* Set the Rx Fifo threshold */
    /* Set fiforxthreshold according the reception data length: 16bit */
    CLEAR_BIT(hspi2.Instance->CR2, SPI_RXFIFO_THRESHOLD);

  /* Check if the SPI is already enabled */
  if ((hspi2.Instance->CR1 & SPI_CR1_SPE) != SPI_CR1_SPE)
  {
    /* Enable SPI peripheral */
    __HAL_SPI_ENABLE(&hspi2);
  }
	
	SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
	
  /* Transmit and Receive data in 16 Bit mode */

	hspi2.Instance->DR = 0;//*((uint16_t *)pTxData);
	/* Check RXNE flag */
	while ( ! __HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_RXNE)){};
	
  *((uint16_t *)pRxData) = hspi2.Instance->DR;
	
	
	SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1
	
  /* Control if the TX fifo is empty */
  if (SPI_WaitFifoStateUntilTimeout(&hspi2, SPI_FLAG_FTLVL, SPI_FTLVL_EMPTY, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }
	

  /* Control the BSY flag */
  if (SPI_WaitFlagStateUntilTimeout(&hspi2, SPI_FLAG_BSY, RESET, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }

	// ERO QUI 23oct19 - SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1
	
	for ( volatile uint16_t i = 0; i < 14 ; i++ )UNUSED(i);


  /* Control if the RX fifo is empty */
  if (SPI_WaitFifoStateUntilTimeout(&hspi2, SPI_FLAG_FRLVL, SPI_FRLVL_EMPTY, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }
	//if (__HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_RXNE))
	//{
	//	*((uint16_t *)pRxData) = hspi2.Instance->DR;
	//}	
  /* Check the end of the transaction */
/*	  if (SPI_EndRxTxTransaction(&hspi2, Timeout, tickstart) != HAL_OK)
  {
    hspi2.ErrorCode = HAL_SPI_ERROR_FLAG;
  }
*/	

	
error :
  hspi2.State = HAL_SPI_STATE_READY;
  __HAL_UNLOCK(&hspi2);

}
