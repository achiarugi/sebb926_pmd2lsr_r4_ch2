/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ON_TC_Pin GPIO_PIN_5
#define ON_TC_GPIO_Port GPIOE
#define WDI_Pin GPIO_PIN_13
#define WDI_GPIO_Port GPIOC
#define I2C2_SDA_Pin GPIO_PIN_0
#define I2C2_SDA_GPIO_Port GPIOF
#define I2C2_SCL_Pin GPIO_PIN_1
#define I2C2_SCL_GPIO_Port GPIOF
#define E2PROM_WP_Pin GPIO_PIN_2
#define E2PROM_WP_GPIO_Port GPIOF
#define TEMP1_Pin GPIO_PIN_0
#define TEMP1_GPIO_Port GPIOC
#define IPELT_Pin GPIO_PIN_1
#define IPELT_GPIO_Port GPIOC
#define TEMP_LASER_Pin GPIO_PIN_2
#define TEMP_LASER_GPIO_Port GPIOC
#define DIFFTEMP_BUF_Pin GPIO_PIN_3
#define DIFFTEMP_BUF_GPIO_Port GPIOC
#define LASER_CATHODE_BUF_Pin GPIO_PIN_4
#define LASER_CATHODE_BUF_GPIO_Port GPIOC
#define LASER_ANODE_BUF_Pin GPIO_PIN_5
#define LASER_ANODE_BUF_GPIO_Port GPIOC
#define TP10_Pin GPIO_PIN_9
#define TP10_GPIO_Port GPIOE
#define ON_VLD_Pin GPIO_PIN_10
#define ON_VLD_GPIO_Port GPIOE
#define LDAC_ACQ_Pin GPIO_PIN_11
#define LDAC_ACQ_GPIO_Port GPIOB
#define SPI2_NSS_Pin GPIO_PIN_12
#define SPI2_NSS_GPIO_Port GPIOB
#define LDAC_TC_Pin GPIO_PIN_7
#define LDAC_TC_GPIO_Port GPIOC
#define LDAC_LASER_Pin GPIO_PIN_8
#define LDAC_LASER_GPIO_Port GPIOC
#define USART1_CTS_Pin GPIO_PIN_11
#define USART1_CTS_GPIO_Port GPIOA
#define USART1_RTS_Pin GPIO_PIN_12
#define USART1_RTS_GPIO_Port GPIOA
#define SWDIO_JTMS_Pin GPIO_PIN_13
#define SWDIO_JTMS_GPIO_Port GPIOA
#define JTCK_SWCLK_Pin GPIO_PIN_14
#define JTCK_SWCLK_GPIO_Port GPIOA
#define JTDI_Pin GPIO_PIN_15
#define JTDI_GPIO_Port GPIOA
#define JTDO_SWO_Pin GPIO_PIN_3
#define JTDO_SWO_GPIO_Port GPIOB
#define JTRST_Pin GPIO_PIN_4
#define JTRST_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
